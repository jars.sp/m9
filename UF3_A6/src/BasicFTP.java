import java.io.IOException;
import java.util.Scanner;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class BasicFTP {
		
		public static void main (String[] args) {
				
				//Servidor FTP
				FTPClient client = new FTPClient(); // Creamos un cliente FTP
				String ServerFTP = "ftp.urv.es"; // Variable para URL del servidor FTP
				System.out.println("Ens connectem al servidor: "+ServerFTP);
				
				//Usuari FTP
				// Pedimos usuario y contrase�a por Scanner
				Scanner teclado = new Scanner(System.in);
				System.out.println("Enter your user name");
				String usuari = teclado.nextLine(); // Variable de usuario
				System.out.println("Enter your password");
				String contrasenya = teclado.nextLine(); // Variable de contrase�a
				
				try {
					
					client.connect(ServerFTP); // Realizamos conexi�n cliente/servidor
					boolean login = client.login(usuari, contrasenya); // Login de usuario
					
					if (login) // Si login es true
						
						System.out.println("Login correcte... ");
						
					else { // Si login es false
						
						System.out.println("Login incorrecte... ");
						client.disconnect(); // Desconectamos el cliente
						System.exit(1);
						
					}
					
					// Se muestra directorio actual
					System.out.println("Directori actual: "+client.printWorkingDirectory());
					// Crea array FTPFile para trabajr con los ficheros del FTP
					FTPFile[] files = client.listFiles();
					System.out.println("Fitxers al directori actual: "+files.length);
					
					//Array par a visualitzar el tipus de fitxer
					String tipus[] = {"Fitxer", "Directori", "Enlla� simbolic"};
					// Guardamos en el array tipus el tipo de fichero y la mostramos
					for (int i=0; i<files.length; i++) {
						
						System.out.println("\t"+files[i].getName()+"=>"+tipus[files[i].getType()]);
						
					}
					// Se realiza logout del cliente
					boolean logout = client.logout();
						
					if (logout) // Si logout true
						
						System.out.println("Logout del servidor FTP... ");
					
					else // Si logout fakse
						
						System.out.println("Error en fer un logout... ");
					
					client.disconnect(); // Se realiza desconexi�n
					System.out.println("Desconnectat... ");
					
				} catch (IOException ioe) {
					
					ioe.printStackTrace();
					
				}
				
				
				
			}



}
