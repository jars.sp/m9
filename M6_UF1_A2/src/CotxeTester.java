import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Scanner;

public class CotxeTester {

	public static void main(String[] args) throws IOException, ClassNotFoundException {
		// variable boolean para el while
		boolean sortir = false;
		// Scanner para introducir par�metros
		Scanner teclado = new Scanner(System.in);
		//Camp variable tipus Cotxe
		Cotxe cotxe;
		//Declaraci� del fitxer
		File fitxer = new File("C:\\Users\\jose\\Desktop\\cotxesObject.txt");
		// while para mostrar men� y elegir entre las diferentes opciones
		while (!sortir) {

			//Connectar el flux de bytes al flux de dades
			ObjectInputStream dataInCotxe = new ObjectInputStream(new FileInputStream(fitxer));
			// Mostramos menu
			System.out.println("MENU");
			System.out.println("1. Guardar un coche");
			System.out.println("2. Mostrar todos los coches");
			System.out.println("3. Buscar coches por parametros");
			System.out.println("4. Salir");
			System.out.println("Elige una opci�n");
			// Variable de opcion de switch
			char opcion = teclado.next().charAt(0);
			teclado.nextLine();

			switch(opcion) {

			case'1': // opci�n 1 introducir nuevo coche
				// Pedimos datos del coche
				System.out.println("Introduce la marca");
				String marca = teclado.nextLine();
				System.out.println("Introduce el modelo");
				String model = teclado.nextLine();
				System.out.println("Introduce el a�o");
				int any = teclado.nextInt();
				teclado.nextLine();
				System.out.println("Introduce la matricula");
				String matricula = teclado.nextLine();
				//Crea el flux de sortida
				FileOutputStream fileout = new FileOutputStream(fitxer);
				//Connectar el flux de bytes al flux de dades
				ObjectOutputStream dataOuCotxe = new ObjectOutputStream(fileout);
				// Se crea objeto cotxe
				cotxe = new Cotxe(marca, model, any, matricula);
				// Se guarda el objeto en el fichero
				dataOuCotxe.writeObject(cotxe);//L'escriu al fixer
				dataOuCotxe.reset();
				dataOuCotxe.close();
				break;

			case'2': // Mostrar coches
				try {

					while (true){//Llegeix el fitxer
						//Llegeix el cotxe
						cotxe = (Cotxe) dataInCotxe.readObject();
						System.out.println("Marca: " +cotxe.getMarca()+ " Model: "+ cotxe.getModel() + 
								" Any: "+ cotxe.getAny() + " Matricula: "+ cotxe.getMatricula());

					}

				} catch (EOFException eo) {}
				break;

			case'3': // Buscar coches por par�metros
				// Mostramos menu y pedimos el par�metro
				System.out.println("MENU");
				System.out.println("1. Marca");
				System.out.println("2. Modelo");
				System.out.println("3. A�o");
				System.out.println("4. Matricula");
				System.out.println("Elige una opci�n");
				char parametro = teclado.next().charAt(0);
				teclado.nextLine();
				boolean checker = false;

				switch(parametro) {

				case'1': // Busqueda por marca
					System.out.println("Introduce la marca");
					marca = teclado.nextLine();
					try {

						while (true){//Llegeix el fitxer
							//Llegeix el cotxe
							cotxe = (Cotxe) dataInCotxe.readObject();
							if (marca.equals(cotxe.getMarca())) { // Comparamos la marca
								System.out.println("Marca: " +cotxe.getMarca()+ " Model: "+ cotxe.getModel() + 
										" Any: "+ cotxe.getAny() + " Matricula: "+ cotxe.getMatricula());
								checker = true;
							}
						}

					} catch (EOFException eo) {}
					dataInCotxe.close();//Tanca el stream d'entrada
					if(!checker) { // Si no hay coincidencias
						System.out.println("No existen coches con ese par�metro");
					}
					break;

				case'2': // Busqueda por modelo
					System.out.println("Introduce el modelo");
					model = teclado.nextLine();
					try {

						while (true){//Llegeix el fitxer
							//Llegeix el cotxe
							cotxe = (Cotxe) dataInCotxe.readObject();
							if (model.equals(cotxe.getModel())) { // Se comprara el modelo
								System.out.println("Marca: " +cotxe.getMarca()+ " Model: "+ cotxe.getModel() + 
										" Any: "+ cotxe.getAny() + " Matricula: "+ cotxe.getMatricula());
								checker = true;
							}
						}

					} catch (EOFException eo) {}
					dataInCotxe.close();//Tanca el stream d'entrada
					if(!checker) { // Si no hay coincidencias
						System.out.println("No existen coches con ese par�metro");
					}
					break;

				case'3': // Busqueda por a�o
					System.out.println("Introduce el a�o");
					any = teclado.nextInt();
					teclado.nextLine();
					try {

						while (true){//Llegeix el fitxer
							//Llegeix el cotxe
							cotxe = (Cotxe) dataInCotxe.readObject();
							if (any == cotxe.getAny()) { // Se compara el a�o
								System.out.println("Marca: " +cotxe.getMarca()+ " Model: "+ cotxe.getModel() + 
										" Any: "+ cotxe.getAny() + " Matricula: "+ cotxe.getMatricula());
								checker = true;
							}
						}

					} catch (EOFException eo) {}
					dataInCotxe.close();//Tanca el stream d'entrada
					if(!checker) { // Si no hay coincidencias
						System.out.println("No existen coches con ese par�metro");
					}
					break;

				case'4': // Busqueda por matricula
					System.out.println("Introduce la matricula");
					matricula = teclado.nextLine();
					try {

						while (true){//Llegeix el fitxer
							//Llegeix el cotxe
							cotxe = (Cotxe) dataInCotxe.readObject();
							if (matricula.equals(cotxe.getMatricula())) { // Se compara la matricula
								System.out.println("Marca: " +cotxe.getMarca()+ " Model: "+ cotxe.getModel() + 
										" Any: "+ cotxe.getAny() + " Matricula: "+ cotxe.getMatricula());
								checker = true;
							}
						}

					} catch (EOFException eo) {}
					dataInCotxe.close();//Tanca el stream d'entrada
					if(!checker) { // Si no hay coincidencias
						System.out.println("No existen coches con ese par�metro");
					}
					break;

				default:
					System.out.println("Opci�n incorrecta");
				}
				break;
			case'4':
				sortir = true;	
				break;
			default:
				System.out.println("Opci�n incorrecta");
			}
		}

	}

}
