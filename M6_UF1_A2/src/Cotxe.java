import java.io.Serializable;

public class Cotxe implements Serializable {
	//Implementa la interf�cie Serializable
	private String marca;
	private String model;
	private int any;
	private String matricula;

	//constructor amb par�metres
	public Cotxe (String marca, String model, int any, String matricula){
		//per no confondre el par�metre amb el camp de variable
		this.marca = marca;
		//per no confondre el par�metre amb el camp de variable
		this.model = model;
		//per no confondre el par�metre amb el camp de variable
		this.any = any;
		//per no confondre el par�metre amb el camp de variable
		this.matricula = matricula;
	}
	public Cotxe (){//constructor per defecte
		this.marca = null;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getModel() {
		return model;
	}
	public void setModel(String model) {
		this.model = model;
	}
	public int getAny() {
		return any;
	}
	public void setAny(int any) {
		this.any = any;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	

}
