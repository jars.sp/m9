import java.io.*;

public class EscriureFitxerObject {
	public static void main(String[] args) throws IOException {
		//Camp variable tipus Cotxe
		Cotxe cotxe;
		//Declaració del fitxer
		File fitxer = new File("C:\\Users\\jose\\Desktop\\comarquesObject.txt");
		//Crea el flux de sortida
		FileOutputStream fileout = new FileOutputStream(fitxer);
		//Connectar el flux de bytes al flux de dades
		ObjectOutputStream dataOuCotxe = new ObjectOutputStream(fileout);
		//Les dades per generar els objectes Cotxe
		String marca[] = {"Citroen", "Peugeot", "Renault", "Ford"};
		String model[] = {"C5 Aircross", "3008", "Megane", "Focus"};
		int any[] = {2019, 2017, 2007, 2018};
		String matricula[] = {"5649JSN", "3685HDP", "6774FXS", "1560HDC"};
		//Recorre els arrays
		for (int i=0; i<marca.length; i++){//Crea el cotxe
			cotxe = new Cotxe(marca[i], model[i], any[i], matricula[i]);
			dataOuCotxe.writeObject(cotxe);//L'escriu al fixer
		}
		dataOuCotxe.close();//Tanca el stream de sortida
	}
}
