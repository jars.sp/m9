import java.net.*;
import java.io.*;

public class ServidorTCP2 {

	public static void main (String[] args) throws Exception {
		
		String client = args[0];
		int numPort = 60000; // Variable para el n�mero del puerto
		ServerSocket servidor = new ServerSocket(numPort); // Socket para el servidor
		
		for (int i = 0; i < Integer.parseInt(client); i++) { // Creamos un for para 3 clientes
			String cadena = "";

			System.out.println("Esperant connexi�... ");
			Socket clientConnectat = servidor.accept(); // Socket para cliente
			System.out.println("Client connectat... ");

			//FLUX DE SORTIDA AL CLIENT
			PrintWriter fsortida = new PrintWriter(clientConnectat.getOutputStream(), true);


			//FLUX D'ENTRADA DEL CLIENT
			BufferedReader fentrada = new BufferedReader(new InputStreamReader(clientConnectat.getInputStream()));

			try { // Guardamos el mensaje del cliente  en la variable cadena
				while ((cadena = fentrada.readLine()) != null) {

					fsortida.println(cadena); // Enviamos la respuesta al cliente
					System.out.println("Rebent: "+cadena); // Imprimimos el mensaje recibido
					if (cadena.contains("bye")) { // Si el mensaje recibio contiene "bye" cerramos conexi�n cliente
						clientConnectat.close(); break;
					}

				}	

				fentrada.close();
				fsortida.close();
			}
			catch (Exception SocketException) {
				System.out.println("Conexi� tancada");
			}
		} // Una vez se cierran las conexines con los 3 clientes cerramos la conexi�n del servidor
		System.out.println("Tancant connexi�... ");
		servidor.close();

	}

}



