import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


public class JavaMailSender {
	// Variables de instancia
	private ArrayList<String> sendTo =new ArrayList<>();
	private String sender;
	private String subject;
	private String body;
	private String[] addresses = new String[1];
	Properties properties=new Properties(); 
	Authenticator authenticator=null;
	// Constructor
	public JavaMailSender(String host, int port) {
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", String.valueOf(port));
	}
	// M�todo autenticaci�n estandar
	public void sendUsingAuthentication(String user, String pass){
		String host = properties.getProperty("mail.smtp.host");
		String port = properties.getProperty("mail.smtp.port");
		properties=new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.auth", "true");
		authenticator = new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication("username","password");
			}
		};
		System.out.println("Realizando autenticaci�n");
		try {
			send();
		} catch (MessagingException e){
			System.out.println("Error...");
		}
	}
	// M�todo para autenticaci�n mediante SSL
	public void sendUsingSSLAuthentication(final String user, final String pass) throws MessagingException {
		String host = properties.getProperty("mail.smtp.host");
		String port = properties.getProperty("mail.smtp.port");
		properties=new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.socketFactory.port", port);
		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.auth", "true");
		authenticator = new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user,pass);
			}
		};
		System.out.println("Realizando autenticaci�n");
		send();
	}
	// M�todo para autenticacion mediante TLS
	public void sendUsingTLSAuthentication(final String user, final String pass) throws MessagingException {
		String host = properties.getProperty("mail.smtp.host");
		String port = properties.getProperty("mail.smtp.port");
		properties=new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.port", port);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
		authenticator = new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user,pass);
			}
		};
		send();
	}
	// M�todos Adds y Setters
	public void addRecipient(String[] recipients) {
		for(String r: recipients){
			sendTo.add(r);
		}
	}

	public void addRecipient(String recipient) {
		sendTo.add(recipient);
	}

	public void setSender(String psender){
		sender = psender;
	}

	public void setMailText(String pbody){
		body = pbody;

	}

	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setReplyTo(String address) {
		this.addresses[0] = address;
	}
	// M�todo de env�o del mensaje
	public void send() throws MessagingException {
		Session session;
		if(authenticator==null) {
			session= Session.getInstance(properties);

		} else {
			session=Session.getInstance(properties, authenticator);

		}

		Message message = new MimeMessage(session);
		InternetAddress[] addresses = new InternetAddress[sendTo.size()];
		for(int i=0; i<sendTo.size(); i++){
			addresses[i]=new InternetAddress(sendTo.get(i));
		}
		message.addRecipients(Message.RecipientType.TO, addresses);
		message.setFrom(new InternetAddress(sender)); // Email remitente
		message.setSentDate(new Date()); // Fecha env�o
		message.setSubject(subject); // Asunto
		message.setText(body); // Texto del cuerpo
		message.setReplyTo(addresses); // Direcciones de respuesta
			
		System.out.println("Realizando autenticaci�n");
		try {
			Transport.send(message);
			System.out.println("Mensaje enviado");
		}
		catch (Exception e) {
			System.err.println("Error de autenticaci�n - Mensaje no enviado");
		}

	}
}



