import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
 
public class SendMailUsingJavaMail {
	
	public static void main(String[] args) {
		try {
			Scanner teclado = new Scanner(System.in);
			// Pedimos email remitente
			System.out.println("Introduce email remitente");
			final String username = teclado.nextLine();
			// Pedimos contrase�a de usuario
			System.out.println("Introduce contrase�a de usuario");
			final String password = teclado.nextLine();
			// Pedimos email de destinatario
			System.out.println("Introduce email de destino");
			final String destinatario = teclado.nextLine();
			// Se crea objeto JavaMailSender para preparaci�n y env�o de mensaje
			JavaMailSender sender = new JavaMailSender("smtp.gmail.com", 465);
			// Se a�ade email al recipiente
			sender.addRecipient(destinatario);
			//sender.addRecipient("algumes@xtec.cat");
			// A�adimos destinatario
			sender.setSender(username);
			// Pedimos el asunto y lo a�adimos
			System.out.println("Introduce asunto del mensaje");
			sender.setSubject(teclado.nextLine());
			// Pedimos texto del cuerpo y los a�adimos
			System.out.println("Introduce el mensaje");
			sender.setMailText(teclado.nextLine());
			sender.setReplyTo("perfectbalance1@hotmail.com");
			// Autendicamos mediante TLS
			//sender.sendUsingTLSAuthentication(username, password);
			
			sender.sendUsingSSLAuthentication(username, password);
			//sender.sendUsingAuthentication(username, password);
			//sender.send();
		} catch (Exception ex) {
			Logger.getLogger(SendMailUsingJavaMail.class.getName()).log(Level.SEVERE, null, ex);
		}
	
	}
	
}


