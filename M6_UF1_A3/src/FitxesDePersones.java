import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Scanner;

public class FitxesDePersones {
	static int posicion = 0;
	public static void main(String[] args) throws IOException {
		boolean sortir = false;

		Scanner teclado = new Scanner(System.in);
		File fitxer = new File("C:/Users/jose/Desktop/persones.txt");
		//Crea un flux (stream) d'arxiu d'acc�s aleatori per llegir
		RandomAccessFile aleatoriFile = new RandomAccessFile(fitxer, "rw");

		while (!sortir) {
			// Mostrem el menu
			System.out.println("MENU");
			System.out.println("1. Guardar persona");
			System.out.println("2. Mostrar persones");
			System.out.println("3. Consultar per id");
			System.out.println("4. Consultar per nom");
			System.out.println("5. Sortir");
			System.out.println("Tria una opcio");
			char opcion = teclado.next().charAt(0);
			teclado.nextLine();
			// switch per triar opcio
			switch(opcion) {
			case'1': // Gaurdar persona
				escriuFitxerAleatori(aleatoriFile);
				break;

			case'2': // Mostrar persones
				lleguirFitxerAleatori(aleatoriFile) ;
				break;

			case'3': // Consultar per id
				consultarFitxerAleatori(aleatoriFile);
				break;

			case'4': // Consultar per nom
				consultarPerNom(aleatoriFile);
				break;

			case'5': // Sortir 
				sortir = true;
				break;

			default: System.out.println("Opci� incorrecta");
			}


		}

	}
	// Metode per a guardar persones
	public static void escriuFitxerAleatori(RandomAccessFile aleatoriFile) throws IOException {
		posicion++;
		Scanner teclado = new Scanner(System.in);
		//Les dades per inserir
		System.out.println("Introdueix nom");
		String nom = teclado.nextLine();
		System.out.println("Introdueix cognoms");
		String cognoms = teclado.nextLine();
		System.out.println("Introdueix telefon");
		int telefon = teclado.nextInt();
		teclado.nextLine();
		System.out.println("Introdueix localitat");
		String localitat = teclado.nextLine();
		System.out.println("Introdueix codi postal");
		int codi = teclado.nextInt();
		teclado.nextLine();

		//Construeix un buffer (mem�ria interm�dia) de strings
		StringBuffer buffer = null;

		aleatoriFile.writeInt(posicion);//1 enter ocupa 4 bytes
		//25 car�cters a 2bytes/car�cter 50 bytes
		buffer = new StringBuffer (nom);
		buffer.setLength(25);
		aleatoriFile.writeChars(buffer.toString());
		//50 car�cters a 2bytes/car�cter 100 bytes
		buffer = new StringBuffer (cognoms);
		buffer.setLength(50);
		aleatoriFile.writeChars(buffer.toString());
		//1 enter ocupa 4 bytes
		aleatoriFile.writeInt(telefon);
		//20 car�cters a 2bytes/car�cter 40 bytes
		buffer = new StringBuffer (localitat);
		buffer.setLength(20);
		aleatoriFile.writeChars(buffer.toString());
		//1 enter ocupa 4 bytes
		aleatoriFile.writeInt(codi);
		//Total 202 bytes
	}
	// Metode per a mostrar el contingut del fitxer
	public static void lleguirFitxerAleatori(RandomAccessFile aleatoriFile) throws IOException {
		//Apuntador s'inicialitza apuntant a l'inici del fitxer
		int apuntador = 0, id;
		int codigo;
		int telefono;
		char noms[] = new char[25]; 
		char cognom[] = new char[50]; 
		char localitats[] = new char[20], aux;

		//Recorrer el fitxer persones
		for (;;) {
			aleatoriFile.seek(apuntador);//Apuntar a l'inici de cada persona al fitxer
			//Llegeix ID
			id = aleatoriFile.readInt();
			//Llegeix Nom
			for(int i = 0; i<noms.length; i++) {
				aux = aleatoriFile.readChar();
				noms[i] = aux;
			}
			String nombre = new String(noms);

			//Llegeix Cognoms
			for(int i = 0; i<cognom.length; i++) {
				aux = aleatoriFile.readChar();
				cognom[i] = aux;
			}
			String apellido = new String(cognom);
			//Llegeix telefon
			telefono = aleatoriFile.readInt();
			//Llegeix Localitat
			for(int i = 0; i<localitats.length; i++) {
				aux = aleatoriFile.readChar();
				localitats[i] = aux;
			}
			String localidad = new String(localitats);
			//Llegeix codi postal
			codigo = aleatoriFile.readInt();
			//Sortida de les dades de cada persona
			System.out.println("ID: "+id+"\nNom: "+nombre+"\nCognoms: "+apellido+"\nTelefon: "+telefono+"\nLocalitat: "+localidad+"\nCP: "+codigo+"\n\n");
			//S'ha de posicionar l'apuntador a la seg�ent persona
			apuntador += 202;
			//Si coincideix on s'est� apuntat amb el final del fitxer, sortim
			if(aleatoriFile.getFilePointer()==aleatoriFile.length()) break;
		}

	}
	// Metode per a consultar per id
	public static void consultarFitxerAleatori(RandomAccessFile aleatoriFile) throws IOException {
		//Apuntador s'inicialitza apuntant a l'inici del fitxer
		int apuntador = 0, seleccio, id;
		int codigo;
		int telefono;
		char noms[] = new char[25]; 
		char cognom[] = new char[50]; 
		char localitats[] = new char[20], aux;

		//Demana a l'usuari que seleccioni la persona pel seu identificador
		System.out.print("Introdueixi el ID de la persona a consultar: ");
		Scanner stdin = new Scanner (System.in);

		seleccio = stdin.nextInt();
		apuntador = (seleccio-1)*202;
		if (apuntador >= aleatoriFile.length()) {
			System.out.println("ERROR: ID incorrecte, no existeix aquesta persona");
		} else {//Apuntar a l'inici del llibre seleccionat al fitxer
			aleatoriFile.seek(apuntador);
			//Llegeix ID
			id = aleatoriFile.readInt();
			//Llegeix Nom
			for(int i = 0; i<noms.length; i++) {
				aux = aleatoriFile.readChar();
				noms[i] = aux;
			}
			String nombre = new String(noms);

			//Llegeix Cognoms
			for(int i = 0; i<cognom.length; i++) {
				aux = aleatoriFile.readChar();
				cognom[i] = aux;
			}
			String apellido = new String(cognom);
			//Llegeix telefon
			telefono = aleatoriFile.readInt();
			//Llegeix Localitat
			for(int i = 0; i<localitats.length; i++) {
				aux = aleatoriFile.readChar();
				localitats[i] = aux;
			}
			String localidad = new String(localitats);
			//Llegeix codi postal
			codigo = aleatoriFile.readInt();
			//Sortida de les dades de cada persona
			System.out.println("ID: "+id+"\nNom: "+nombre+"\nCognoms: "+apellido+"\nTelefon: "+telefono+"\nLocalitat: "+localidad+"\nCP: "+codigo+"\n\n");

		}
	}
	// Metode per a consultar per nom
	public static void consultarPerNom(RandomAccessFile aleatoriFile) throws IOException {
		//Apuntador s'inicialitza apuntant a l'inici del fitxer
		int apuntador = 0, id;
		String seleccio;
		int codigo;
		int telefono;
		char noms[] = new char[25]; 
		char cognom[] = new char[50]; 
		char localitats[] = new char[20], aux;

		//Demana a l'usuari que seleccioni la persona pel seu nom
		System.out.print("Introdueixi el Nom de la persona a consultar: ");
		Scanner stdin = new Scanner (System.in);

		seleccio = stdin.nextLine();
		for (;;) {
			aleatoriFile.seek(apuntador);

			//Llegeix ID
			id = aleatoriFile.readInt();
			//Llegeix Nom
			for(int i = 0; i<noms.length; i++) {
				aux = aleatoriFile.readChar();
				noms[i] = aux;
			}
			String nombre = new String(noms);

			//Llegeix Cognoms
			for(int i = 0; i<cognom.length; i++) {
				aux = aleatoriFile.readChar();
				cognom[i] = aux;
			}
			String apellido = new String(cognom);
			//Llegeix telefon
			telefono = aleatoriFile.readInt();
			//Llegeix Localitat
			for(int i = 0; i<localitats.length; i++) {
				aux = aleatoriFile.readChar();
				localitats[i] = aux;
			}
			String localidad = new String(localitats);
			//Llegeix codi postal
			codigo = aleatoriFile.readInt();
			// Comparem els noms
			if (seleccio.equals(nombre.trim())) {
				//Sortida de les dades de cada persona
				System.out.println("ID: "+id+"\nNom: "+nombre+"\nCognoms: "+apellido+"\nTelefon: "+telefono+"\nLocalitat: "+localidad+"\nCP: "+codigo+"\n\n");
			} 
			//S'ha de posicionar l'apuntador a la seg�ent persona
			apuntador += 202;
			//Si coincideix on s'est� apuntat amb el final del fitxer, sortim
			if(aleatoriFile.getFilePointer()==aleatoriFile.length()) break;
		}
	}

}
