import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class Exemple2URL {

	public static void main (String[] args) {
		// Declaramos variable URL y a�adimos los argumentos
		URL url=null;
		String protocol = args[0];
		String authority = args[1];
		String port = args[2];
		String file = args[3];

		try {

			url = new URL(protocol, authority, Integer.parseInt(port), file);

		} catch (MalformedURLException e) {e.printStackTrace(); }

		BufferedReader in;

		try {
			// Abre una conexi�n a la URL y retorna un InputStream de la misma
			InputStream inputStream = url.openStream();
			// Declaramos un BufferedReader para leer el contenido del inputStream
			in = new BufferedReader(new InputStreamReader(inputStream));

			String inputLine;		
			// Leemos el InputStream l�nea por l�nea,
			// si tiene contenido la mostramos
			while ((inputLine = in.readLine()) != null)
				System.out.println(inputLine);
			in.close();

		} catch (IOException e) {e.printStackTrace(); }

	}
}
