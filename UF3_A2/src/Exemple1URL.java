import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

public class Exemple1URL {
	
	public static void main (String[] args) throws IOException {
		URL url;
		String protocol = args[0];
		String authority = args[1];
		String port = args[2];
		String file = args[3];
		
		try {
			
			System.out.println("Constructor per a protocol + URL + port + directori: ");
			url = new URL(protocol, authority, Integer.parseInt(port), file);
			Visualitzar (url);
			
			System.out.println("Constructor simple per a un URL: ");
			url = new URL("http://docs.oracle.com/");
			Visualitzar (url);
			
			System.out.println("Altra constructor simple per a un URL: ");
			url = new URL("http://localhost/moodle/");
			Visualitzar (url);
			
			System.out.println("Constructor per a protocol + URL + directori: ");
			url = new URL("http", "doc.oracle.com", "/javase/7");
			Visualitzar (url);
			
			System.out.println("Constructor per a protocol + URL + port + directori: ");
			url = new URL("http", "doc.oracle.com", 80, "/javase/7");
			Visualitzar (url);
			
			System.out.println("Constructor per a un objecte URL i un directori: ");
			URL urlBase = new URL("http://docs.oracle.com/");
			url = new URL(urlBase, "/javase/7/docs/api/java/net/URL.html");
			Visualitzar (url);
			
		} catch (MalformedURLException e) { System.out.println(e); }
		
	}
	
	private static void Visualitzar(URL url) throws IOException {
		
		System.out.println("\tURL complerta: "+url.toString() + " (Muestra la URL en formato String)");
		System.out.println("\tgetProtocol: "+url.getProtocol() + " (Obtiene el protocolo de la URL http/https)");
		System.out.println("\tgetHost: "+url.getHost() + " (Obtiene el nombre del host de la URL)");
		System.out.println("\tgetPort: "+url.getPort() + " (Obtiene el n�mero de puerto de la URL)");
		System.out.println("\tgetFile: "+url.getFile() + " (Retorna el nombre del fichero de la URL)");
		System.out.println("\tgetUserInfo: "+url.getUserInfo() + " (Retorna la informaci�n del ususario actual de la URL)");
		System.out.println("\tgetPath: "+url.getPath() + " (Retorna la ruta de la URL)");
		System.out.println("\tgetAuthority: "+url.getAuthority() + " (Retorna la parte de autoridad de la URL)");
		System.out.println("\tgetQuery: "+url.getQuery() + " (Retorna la pare de la query de la URL, si existe)");
		System.out.println("\topenConnection: "+url.openConnection() + " (Retorna una instancia de conexi�n a la URL)");
		System.out.println("=====================================================");
	}

}

