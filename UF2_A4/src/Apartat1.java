import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class Apartat1 {

	static class Client implements Callable<int[]> {
		private int operador;
		private int numClient;

		// Se crea constructor de la clase
		public Client(int operador, int numClient) {
			this.operador = operador;
			this.numClient = numClient;
		}

		// Se crea el m�todo call para ejecutar las tareas a trav�s de los hilos
		@Override
		public int[] call() throws Exception {

			Thread.sleep(3000);

			int[] client = new int[operador];

			for (int i = 0; i < client.length; i++) {

				client[i] = (int)(Math.random()*30+1);

			}

			return client;
		}

	}

	static class Caixa implements Callable<int[]> {

		private int client;
		private int numClient;
		private int article;
		private static int numCaixa;

		public Caixa(int client, int numClient, int article) {
			this.client = client;
			this.numClient = numClient;
			this.article = article;
		}

		@Override
		public int[] call() throws Exception {
			
			numCaixa++;
			// Usamos un ThreadSleep random entre 2 y 8 segundos
			Random rn = new Random();
			int caixa = rn.nextInt((7500 - 2000) + 1000) + 2000;
			double tInicial = System.currentTimeMillis();
			try {
				Thread.sleep(caixa);
			} catch (InterruptedException e) {

				e.printStackTrace();
			}

			// Imprimimos los articulos conforme pasan por caja
				System.out.print("Client " + numClient + " article " + article +"/" + client + " ");
				System.out.println("(" + Math.round((System.currentTimeMillis()-tInicial)/1000) + " segons)...");
				//System.out.println(Thread.currentThread().getName());
				
				
			return null;
		}

	}

	public static void main(String[] args) throws InterruptedException, ExecutionException {

		// Con el objeto ThreadPoolExecutor de tipo newFixedThreadPool()
		// indicamos el n�mero de hilos que queremos para ejecutar las tareas
		ThreadPoolExecutor executorClient = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);

		// Creamos lista para almacenar las tareas
		List<Client> llistaClient= new ArrayList<Client>();

		// Generamos objetos Client (tareas) y se a�aden a las lista
		for (int i = 0; i < 3; i++) {
			int random = (int)(Math.random()*30+1);
			Client client = new Client(random, i+1);
			llistaClient.add(client);

		}

		// Creamos una lista Future para almacenar los resultados
		// una vez se ejecuten las tareas
		List <Future<int[]>> llistaResultatsClients;

		// Ejecutamos "todos los hilos" con invokeAll y se guardan los resultados
		llistaResultatsClients = executorClient.invokeAll(llistaClient);

		// Paramos el ejecutor para matar los hilos
		executorClient.shutdown();
		
		
		// Con el objeto ThreadPoolExecutor de tipo newFixedThreadPool()
		// indicamos el n�mero de hilos que queremos para ejecutar las tareas
		ThreadPoolExecutor executorCaixa = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);

		// Creamos lista para almacenar las tareas
		List<Caixa> llistaCaixes= new ArrayList<Caixa>();
		
		// Declaramos array para guardar los resultados de clientes
		Future<int[]> resultatClients = null;

		// Generamos objetos Client (tareas) y se a�aden a las lista
		for (int i = 0; i < llistaResultatsClients.size(); i++) {

			resultatClients = llistaResultatsClients.get(i);

			// Imprimimos los clientes con sus respectivos articulos
			System.out.print("Creat el client " + (i+1) + " amb " + resultatClients.get().length+ " articles (");

			for (int j = 0; j < resultatClients.get().length; j++) {

				System.out.print(resultatClients.get()[j] + " ");

				Caixa caixa = new Caixa(resultatClients.get().length,(i+1),(j+1));
				llistaCaixes.add(caixa);

			}
			System.out.println(")");
			System.out.println("Client " + (i+1) + " passa per caixa...");
		}
		
		// Creamos una lista Future para almacenar los resultados
		// una vez se ejecuten las tareas
		List <Future<int[]>> llistaResultatsCaixes;

		// Ejecutamos "todos los hilos" con invokeAll y se guardan los resultados
		llistaResultatsCaixes = executorCaixa.invokeAll(llistaCaixes);
		
		// Paramos el ejecutor para matar los hilos
		executorCaixa.shutdown();

	}

}


