import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

// Clase recursiva para calcular numeros catalanes
public class Apartat3_C extends RecursiveTask<Integer>{

	// Variable numero de clase
	private int numero;
	
	// Constructor de clase
	public Apartat3_C(int numero) {
		
		this.numero = numero;
	}

	// Metodo catalan recursivo sin hilos
	public static int catalan(int numero) {

		int res = 0; 

		// Base case 
		if (numero <= 1) { 
			return 1; 
		} else {

			for (int i = 0; i < numero; i++) { 
				res += catalan(i) * catalan(numero - 1 - i);
			} 
		}
		return res; 
	}
	
	// Metodo recursivo catalan con hilos ForkJoin
	@Override
	protected Integer compute() {

		int res = 0; 

		// Base case 
		if (numero <= 1) { 
			return 1; 
		} 
		
		int i = 0;
		
		Apartat3_C tasca1;
		Apartat3_C tasca2;
		
		for (i = 0; i < numero; i++) { 
			tasca1 = new Apartat3_C(i);
			tasca1.fork();
			tasca2 = new Apartat3_C(numero - 1 - i);
			tasca2.fork();
			res +=  tasca1.join() * tasca2.join();
			
		} 
		return res; 
	}

	public static void main(String[] args) {

		// Pedimos por teclado el limite de secuencia
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce el l�mite de secuencia");
		int numero = teclado.nextInt();
		teclado.nextLine();
		
		// Invocamos al metodo sin hilos
		double start = System.currentTimeMillis();
		
		for (int i = 0; i < numero; i++) {
			
			System.out.print(catalan(i) + " ");
		}
		
		double end = System.currentTimeMillis();
		System.out.println("Tiempo transcurrido sin hilos " + (end - start) / 1000);

		// Invocamos al metodo con hilos
		double startFils = System.currentTimeMillis();
		ForkJoinPool pool = new ForkJoinPool();
		
		for (int i = 0; i < numero; i++) {
			System.out.print(pool.invoke(new Apartat3_C(i)) + " ");
		}
		
		double endFils = System.currentTimeMillis();
		System.out.println("Tiempo transcurrido con hilos " + (endFils - startFils) / 1000);
	}

}
