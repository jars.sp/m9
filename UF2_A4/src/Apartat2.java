
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

// Clase de ordenaci�n burbuja
public class Apartat2 extends RecursiveTask<int[]> {
	
	// Variables de clase
	private static int contador;
	private static int[] array;
	private  int inici;
	private int fi;
	
	// Constructor de clase
	public Apartat2(int[] array, int inici, int fi) {
		
		this.array = array;
		this.inici = inici;
		this.fi = fi;
		
	}

	// Metodo compute() para ordenacion burbuja por bloques
	@Override
	protected int[] compute() {
		contador++;
		System.out.print("Bloque "+ contador + "  ");
		
		// Divimos el array seg�n procesadores o n�cleos
		Apartat2 task1;
		Apartat2 task2;
		int mig = (inici+fi)/2+1;
		task1 = new Apartat2(array, inici, mig);
		task1.fork();
		task2 = new Apartat2(array, mig, fi);
		task2.fork();
		
		// Ordenamos bloques por metodo burbuja
		for(int i = 0; i < array.length - 1; i++)
        { 
            for(int j = 0; j < array.length - 1; j++)
            {
                if (array[j] < array[j + 1])
                {
                    int tmp = array[j+1];
                    array[j+1] = array[j];
                    array[j] = tmp;
                    
                } 
            }
        }
		  
	        return array;
	}
	
	// Metodo para crear array con datos introducidos por teclado
	private static int [] createArray(int tam) throws NumberFormatException, IOException{
	
		Scanner teclado = new Scanner(System.in);	
		/*creacion del array*/
		int arr[] = new int[tam];
		System.out.println();
		
		/*lectura del array*/
		int j = 0;
		for (int i = 0 ; i < arr.length;i++)
		{
			j+=1;
			System.out.print("Elemento " + j + " : ");
			arr[i] = teclado.nextInt();
		}
		teclado.close();
		return arr;
	}
	

	public static void main(String[] args) throws NumberFormatException, IOException {

		// Pedimos datos a introducir
		Scanner teclado = new Scanner(System.in);
		System.out.println("Total de datos a introducir : ");
		int tam = teclado.nextInt();
		teclado.nextLine();
		
		// Creamos el array donde se guardaranlos datos
		int[] arr = createArray(tam);
		
		// Creamos un pool que detectara los procesadores/nucleos
		ForkJoinPool pool = new ForkJoinPool();
		int inici= 0;
		int fi= arr.length;
		
		// Invocamos la tarea
		Apartat2 tasca = new Apartat2(arr,inici,fi);
		pool.invoke(tasca);
		
		// Guardamos los resultados y se imprimen
		int[] result= tasca.join();
		for (int i = result.length-1; i >= 0; i--) {
			
			System.out.print(result[i] + " ");
		}
		
		teclado.close();
	}


}
