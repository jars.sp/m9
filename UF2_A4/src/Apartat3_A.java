import java.util.concurrent.ForkJoinPool;

import java.util.concurrent.RecursiveTask;

import java.util.Scanner;

// Clase Aparatado 3a potencia recursiva
public class Apartat3_A extends RecursiveTask<Double> {

	// Variables de clase
	private double base;
	private double exponente;

	// Constructor de clase
	public Apartat3_A(double base, double exponente) {
		
		this.base = base;
		this.exponente = exponente;
	}
	
	// Metodo potencia recursiva sin hilos ForkJoin
	public static double potencia(double base, double exponente) {
		
		if(exponente == 0) {
			
			return 1;
			
		} else {
			double calcul = java.lang.Math.cos(54879854);
			
			return base * potencia(base, exponente-1);
		}
	}
	
	// Metodo potencia recursiva con ForkJoin
	@Override
	protected Double compute() {
		
	if(exponente == 0) {
			
			return (double) 1;
			
		} else {
			
			double calcul = java.lang.Math.cos(54879854);
			Apartat3_A tasca = new Apartat3_A(base,exponente-1);
			tasca.fork();
			
			return base * tasca.join();
		}
		
	}
	
	public static void main(String[] args) {
		
		Scanner teclado = new Scanner(System.in);
		
		// Pedimos la base y el exponente por teclado
		System.out.println("Introduce la base");
		double base = teclado.nextDouble();
		System.out.println("Introduce el exponente");
		double exponente = teclado.nextDouble();
		
		// Invocamos al metodo sin hilos
		double start = System.currentTimeMillis();
		System.out.println(potencia(base,exponente));
		double end = System.currentTimeMillis();
		System.out.println("Tiempo transcurrido sin hilos " + (end - start));
		
		// Invocamos al metodo con hilos
		double startFils = System.currentTimeMillis();
		ForkJoinPool pool = new ForkJoinPool();
		System.out.println(pool.invoke(new Apartat3_A(base,exponente)));
		double endFils = System.currentTimeMillis();
		System.out.println("Tiempo transcurrido con hilos " + (endFils - startFils));
		
	}


}
