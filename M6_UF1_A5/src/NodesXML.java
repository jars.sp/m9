import java.io.File;	
import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;


public class NodesXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {

		// Abrimos Scanner para datos por teclado
		Scanner teclado = new Scanner(System.in);
		// per a carregar en mem�ria un arxiu xml
		File file = new File("alumnes.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		// Creamos objeto de clase para poder invocar el metodo idAttributeTrue();
		NodesXML node = new NodesXML();
		node.idAttributeTrue(doc);
		Element root = doc.getDocumentElement();
		// Declaramos boolean para controlar el while
		boolean sortir = false;

		while (!sortir) {
			// Mostramos el menun de opciones
			System.out.println("MEN�");
			System.out.println("1. A�adir elemento");
			System.out.println("2. Eliminar elemento");
			System.out.println("3. Modificar elemento");
			System.out.println("4. Mostrar fichero");
			System.out.println("5. Gesti�n de atributos");
			System.out.println("6. Salir");
			char opcion = teclado.next().charAt(0);

			switch(opcion) {
			case '1': // Opcion 1 a�adir elemento
				addElement(doc, node);
				break;
			case '2': // Opcion 2 eliminar elemento
				eliminarElement(doc, node);
				break;
			case '3': // Opcion 3 modificar elemento
				modificarElement(doc, node);
				break;
			case '4': // Opcion 4 mostrar el contenido del fichero
				String espai = "  ";
				mostrarNodes(root, espai);
				break;
			case'5': // Opcion 5 administracion de atributos
				attributesAdmin(doc, node);
				break;
			case '6': // Opcion 6 salir del programa
				sortir = true;
				break;
			default: // Por defecto mostramos mensaje
				System.out.println("Opci�n incorrecta");
				break;
			}

		}

	}
	// Metodo para a�adir elemento
	public static void addElement(Document doc, NodesXML node) throws TransformerException {
		int id = node.getLastId(doc); // Obtenemos la ultima id de todos los elementos
		// Pedimos los datos de todos los elementos
		Scanner teclado = new Scanner(System.in);
		System.out.println("Nombre");
		String nombre = teclado.nextLine();
		//teclado.nextLine();
		System.out.println("Apellido1");
		String apellido1 = teclado.nextLine();
		System.out.println("Apellido2");
		String apellido2 = teclado.nextLine();
		System.out.println("Nota final");
		String nota = teclado.nextLine();

		Element nodeArrel = doc.getDocumentElement();
		Element addAlumne = doc.createElement("alumne"); // Creamos elemento alumne
		addAlumne.setAttribute("id",Integer.toString(id+1)); // Le asignamos el atributo id
		nodeArrel.appendChild(addAlumne); // A�adimos el elmento alumne
		addAlumne.setIdAttribute("id", true);
		// Creamos todos los elementos hijos
		Element nom = doc.createElement("nom"); 
		Element cognom1 = doc.createElement("cognom1"); 
		Element cognom2 = doc.createElement("cognom2"); 
		Element notaFinal = doc.createElement("notaFinal"); 
		// Asignamos contenido de los elementos
		nom.appendChild(doc.createTextNode(nombre));
		cognom1.appendChild(doc.createTextNode(apellido1));
		cognom2.appendChild(doc.createTextNode(apellido2));
		notaFinal.appendChild(doc.createTextNode(nota));
		// A�adimos los elementos
		addAlumne.appendChild(nom);
		addAlumne.appendChild(cognom1);
		addAlumne.appendChild(cognom2);
		addAlumne.appendChild(notaFinal);
		// Preguntamos confirmacion de modificaciones
		confirmarModificaciones(doc, node, teclado);

	}
	// Metodo para eliminar elementos
	public static void eliminarElement(Document doc, NodesXML node) throws TransformerException {

		Scanner teclado = new Scanner(System.in);
		// Pedimos elemento raiz
		System.out.println("Id del elemento raiz");
		String id = teclado.nextLine();
		int count = 1;
		Element element = doc.getElementById(id);
		NodeList nodeList = element.getChildNodes();
		// Preguntamos por el elemento a eliminar
		// Mostramos la lista de nodos hijos del elemento raiz
		System.out.println("�Qu� elemento desea eleminiar? ");
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i) instanceof Text) {

			} else {
				System.out.println((count) + ". " + nodeList.item(i).getNodeName() + ": " + nodeList.item(i).getTextContent());
				count++;
			}
		}

		System.out.println(count + ". Alumne " + id);
		// Guardamos la opcion elegida
		char opcion = teclado.next().charAt(0);
		teclado.nextLine();
		count = 1;
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i) instanceof Text) { // Si el nodo est� vac�o no hacemos nada

			} else  { 
				// Si el nodo no esta vacio comprobamos si coincide la opci�n con el nodo correspondiente
				// Eliminamos elemento
				if (Integer.parseInt(String.valueOf(opcion)) == count) {
					nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
					confirmarModificaciones(doc, node, teclado);
				} 
				count++;
			}

		}
		// Si la opci�n coincide con la ultima posicion de la elecciones posibles
		// Eliminamos el elmento raiz
		if (Integer.parseInt(String.valueOf(opcion)) == count) {
			element.getParentNode().removeChild(element);
			confirmarModificaciones(doc, node, teclado);
		} 

	}

	// Metodo para modificar elementos
	public static void modificarElement(Document doc, NodesXML node) throws TransformerException {
		Scanner teclado = new Scanner(System.in);
		// Pedimos id del elemento raiz
		System.out.println("Id del elemento a modificar");
		String id = teclado.nextLine();
		int count = 1;
		Element element = doc.getElementById(id);
		NodeList nodeList = element.getChildNodes();
		// Mostramos los hijos del elemento raiz
		System.out.println("Nodos hijos del elemento Alumne " + id);
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i) instanceof Text) {

			} else {
				System.out.println((count) + ". " + nodeList.item(i).getNodeName() + ": " + nodeList.item(i).getTextContent());
				count++;
			}
		}
		// Pedimos el elemento a modificar
		System.out.println("�Qu� elemento desea modificar?");
		char opcion = teclado.next().charAt(0);
		teclado.nextLine();

		switch(opcion) {
		case'1': // Opcion 1 modificamos nom
			for (int i = 0; i < nodeList.getLength(); i++) {
				if (nodeList.item(i).getNodeName().equals("nom")) {
					nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
					System.out.println("Introduce nuevo nombre");
					String nom = teclado.nextLine();
					Element nomNode = doc.createElement("nom");
					nomNode.setTextContent(nom);
					element.insertBefore(nomNode, nodeList.item(i));
					confirmarModificaciones(doc, node, teclado);
				}
			}
			break;
		case'2': // Opcion 2 modificamos cognom1
			for (int i = 0; i < nodeList.getLength(); i++) {
				if (nodeList.item(i).getNodeName().equals("cognom1")) {
					nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
					System.out.println("Introduce nuevo cognom1");
					String cognom1 = teclado.nextLine();
					Element nomNode = doc.createElement("cognom1");
					nomNode.setTextContent(cognom1);
					element.insertBefore(nomNode, nodeList.item(i));
					confirmarModificaciones(doc, node, teclado);
				}
			}
			break;
		case'3': // Opcion 3 modificamos cognom2
			for (int i = 0; i < nodeList.getLength(); i++) {
				if (nodeList.item(i).getNodeName().equals("cognom2")) {
					nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
					System.out.println("Introduce nuevo cognom2");
					String cognom2 = teclado.nextLine();
					Element nomNode = doc.createElement("cognom2");
					nomNode.setTextContent(cognom2);
					element.insertBefore(nomNode, nodeList.item(i));
					confirmarModificaciones(doc, node, teclado);
				}
			}
			break;
		case'4': // Opcion 4 modificamos notaFinal
			for (int i = 0; i < nodeList.getLength(); i++) {
				if (nodeList.item(i).getNodeName().equals("notaFinal")) {
					nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
					System.out.println("Introduce nueva notaFinal");
					String notaFinal = teclado.nextLine();
					Element nomNode = doc.createElement("notaFinal");
					nomNode.setTextContent(notaFinal);
					element.insertBefore(nomNode, nodeList.item(i));
					confirmarModificaciones(doc, node, teclado);
				}
			}
			break;
		default: // Por defecto mostramos mensaje
			System.out.println("Opci�n incorrecta");
		}
	}
	// Metodo para mostrar el contenido del fichero
	public static void mostrarNodes(Element root, String espai) {
		//Element root = doc.getDocumentElement();
		NodeList nodeList = root.getChildNodes(); // Lista para guardar los nodos
		NamedNodeMap attrs;
		String nodeContent = null; // Variable para guardar el contenido de los nodos

		// Recorremos los nodos del elemento raiz
		for (int i = 0; i < nodeList.getLength(); i++) {
			attrs = nodeList.item(i).getAttributes(); // Guardamos los atributos del nodo
			if (nodeList.item(i) instanceof Text) { // Comprobamos si el nodo esta vacio

			} else {
				if (nodeList.item(i).getNodeName().equals("alumne")) { // Si el nodo es alumne mostramos nombre y su id
					String id = attrs.getNamedItem("id").getNodeValue();
					System.out.println(nodeList.item(i).getNodeName() + " id: " + id);
					
				} else { // Si el nodo es de otro tipo mostramos su nombre y su contenido
					System.out.println(espai + nodeList.item(i).getNodeName() + ": " + nodeList.item(i).getTextContent());
				}

				// Guardamos elemento del nodo e invocamos metodo de nuevo
				Element e =(Element) nodeList.item(i);
				mostrarNodes(e,espai+espai);	
			}
		}
	}
	// Metodo para administrar los atributos (a�adir, modificar o eliminar)
	public static void attributesAdmin(Document doc, NodesXML node) throws TransformerException {
		Scanner teclado = new Scanner(System.in);
		// Pedimos id del elemento raiz
		System.out.println("Id del elemento");
		String id = teclado.nextLine();
		Element element = doc.getElementById(id);
		// Guardamos atributos del elemento
		NamedNodeMap attrs = element.getAttributes();
		// Mostramos los atributos
		System.out.println("Atributos del elemento");
		for (int i = 0; i < attrs.getLength(); i++) {
			System.out.println(attrs.item(i).getNodeName() + ": " + attrs.item(i).getNodeValue());
		}
		// Pedimos la opcion a realizar
		System.out.println("Elija una opci�n");
		System.out.println("1. A�adir o modificar atributo");
		System.out.println("2. Eliminar atributo");
		char opcion = teclado.next().charAt(0);
		teclado.nextLine();

		switch(opcion) {

		case'1': // Opcion 1 a�adir o modificar atributo
			System.out.println("Nombre del atributo a a�adir o modificar");
			String nom = teclado.nextLine();
			System.out.println("Valor del atributo");
			String valor = teclado.nextLine();	
			element.setAttribute(nom, valor);
			confirmarModificaciones(doc, node, teclado);
			break;
		case'2': // Opcion 2 eliminar atributo
			System.out.println("Nombre del atributo a eliminar");
			nom = teclado.nextLine();	
			element.removeAttribute(nom);
			confirmarModificaciones(doc, node, teclado);
			break;
		default: // Por defecto mostramos mensaje
			System.out.println("Opci�n incorrecta");

		}

	}
	// Metodo para aplicar la funcion setIdAttribute() a todos los elementos del fichero
	public static void idAttributeTrue(Document doc) {
		Element root = doc.getDocumentElement();
		NodeList nodeList = root.getChildNodes(); // Lista para guardar los nodos
		Element e = null;

		for (int i = 0; i < nodeList.getLength(); i++) {
			if("alumne".equals(nodeList.item(i).getNodeName())) {	
				e = (Element)nodeList.item(i);
				e.setIdAttribute("id", true);
			}
		}
	}
	// Metodo que devuelve la id del ultimo elemento del fichero
	public static int getLastId(Document doc) {
		Element root = doc.getDocumentElement();
		NodeList nodeList = root.getChildNodes(); // Lista para guardar los nodos
		String id = null;

		for (int i = 0; i < nodeList.getLength(); i++) {
			NamedNodeMap attrs = nodeList.item(i).getAttributes();
			if("alumne".equals(nodeList.item(i).getNodeName())) {	
				id = attrs.getNamedItem("id").getNodeValue();
			}
		}

		return Integer.parseInt(id);
	}
	// Metodo para confirmar las modificaciones a realizar en el fichero
	public static void confirmarModificaciones(Document doc, NodesXML node, Scanner teclado) throws TransformerException {
		while (true) {
			System.out.println("�Quieres confirmar modificaciones? (SI/NO)");
			String respuesta = teclado.nextLine();
			if (respuesta.equalsIgnoreCase("SI")) {
				saveData(doc,node); 	
				return;
			} else if (respuesta.equalsIgnoreCase("NO")) {
				return;
			} else {
				System.out.println("Opci�n incorrecta");
			}
		}
	}
	// Metodo para guardar las modificaciones en el fichero
	public static void saveData(Document doc, NodesXML node) throws TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();
		DOMSource domSource = new DOMSource(doc);
		StreamResult streamResult = new StreamResult(new File("alumnes.xml"));
		transformer.transform(domSource, streamResult);
		//DOMSource source = new DOMSource(doc);

	}

}
