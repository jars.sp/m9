import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class NodesXML2 {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, TransformerException {
		// Abrimos Scanner para datos por teclado
		Scanner teclado = new Scanner(System.in);
		// per a carregar en mem�ria un arxiu xml
		File file = new File("oficines2.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);
		// Creamos objeto de clase para poder invocar el metodo idAttributeTrue();
		NodesXML2 node = new NodesXML2();
		node.idAttributeTrue(doc);
		Element root = doc.getDocumentElement();
		// Declaramos boolean para controlar el while
		boolean sortir = false;
		String espai = "  ";

		while (!sortir) {
			// Mostramos el menun de opciones
			System.out.println("MEN�");
			System.out.println("1. A�adir elemento");
			System.out.println("2. Eliminar elemento");
			System.out.println("3. Mostrar elementos");
			System.out.println("4. Salir");
			char opcion = teclado.next().charAt(0);

			switch(opcion) {
			case '1': // Opcion 1 a�adir elemento
				addElement(doc, node);
				break;
			case '2': // Opcion 2 eliminar elemento
				eliminarElement(doc, node);
				break;
			case '3':
				mostrarNodes(root, espai);
				break;
			case '4': // Opcion 3 salir del programa
				sortir = true;
				break;
			default: // Por defecto mostramos mensaje
				System.out.println("Opci�n incorrecta");
				break;
			}

		}

	}
	// Metodo para a�adir elemento
	public static void addElement(Document doc, NodesXML2 node) throws TransformerException {
		int id = node.getLastId(doc); // Obtenemos la ultima id de todos los elementos
		// Pedimos los datos de todos los elementos
		Scanner teclado = new Scanner(System.in);
		System.out.println("Tipo de informaci�n");
		String tipo = teclado.nextLine();
		//teclado.nextLine();
		System.out.println("C�digo de informacion");
		String codigo = teclado.nextLine();
		System.out.println("Departamento");
		String departamento = teclado.nextLine();
		System.out.println("Unidad");
		String unidad = teclado.nextLine();

		Element nodeArrel = doc.getDocumentElement();
		Element addRow = doc.createElement("row"); // Creamos elemento row
		addRow.setAttribute("id",Integer.toString(id+1)); // Le asignamos el atributo id
		addRow.setAttribute("_position", "0"); // Le asignamos el atributo _position
		addRow.setAttribute("_address","https://analisi.transparenciacatalunya.cat"); // Le asignamos el atributo _address
		addRow.setIdAttribute("id", true);
		nodeArrel.appendChild(addRow); // A�adimos el elmento row
		
		// Creamos todos los elementos hijos
		Element tipus = doc.createElement("tipus_d_informaci"); 
		Element codi = doc.createElement("codi_d_informaci"); 
		Element departament = doc.createElement("departament"); 
		Element unitat = doc.createElement("unitat"); 
		// Asignamos contenido de los elementos
		tipus.appendChild(doc.createTextNode(tipo));
		codi.appendChild(doc.createTextNode(codigo));
		departament.appendChild(doc.createTextNode(departamento));
		unitat.appendChild(doc.createTextNode(unidad));
		// A�adimos los elementos
		addRow.appendChild(tipus);
		addRow.appendChild(codi);
		addRow.appendChild(departament);
		addRow.appendChild(unitat);
		// Preguntamos confirmacion de modificaciones
		confirmarModificaciones(doc, node, teclado);

	}
	// Metodo para eliminar elementos
	public static void eliminarElement(Document doc, NodesXML2 node) throws TransformerException {

		Scanner teclado = new Scanner(System.in);
		// Pedimos elemento raiz
		System.out.println("Id del elemento raiz");
		String id = teclado.nextLine();
		int count = 1;
		Element element = doc.getElementById(id);
		NodeList nodeList = element.getChildNodes();
		// Preguntamos por el elemento a eliminar
		// Mostramos la lista de nodos hijos del elemento raiz
		System.out.println("�Qu� elemento desea eleminiar? ");
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i) instanceof Text) {

			} else {
				System.out.println((count) + ". " + nodeList.item(i).getNodeName() + ": " + nodeList.item(i).getTextContent());
				count++;
			}
		}

		System.out.println(count + ". Row " + id);
		// Guardamos la opcion elegida
		char opcion = teclado.next().charAt(0);
		teclado.nextLine();
		count = 1;
		for (int i = 0; i < nodeList.getLength(); i++) {
			if (nodeList.item(i) instanceof Text) { // Si el nodo est� vac�o no hacemos nada

			} else  { 
				// Si el nodo no esta vacio comprobamos si coincide la opci�n con el nodo correspondiente
				// Eliminamos elemento
				if (Integer.parseInt(String.valueOf(opcion)) == count) {
					nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
					confirmarModificaciones(doc, node, teclado);
				} 
				count++;
			}

		}
		// Si la opci�n coincide con la ultima posicion de la elecciones posibles
		// Eliminamos el elmento raiz
		if (Integer.parseInt(String.valueOf(opcion)) == count) {
			element.getParentNode().removeChild(element);
			confirmarModificaciones(doc, node, teclado);
		} 

	}
	// Metodo para mostrar el contenido del fichero
		public static void mostrarNodes(Element root, String espai) {
			//Element root = doc.getDocumentElement();
			NodeList nodeList = root.getChildNodes(); // Lista para guardar los nodos
			NamedNodeMap attrs;
			String nodeContent = null; // Variable para guardar el contenido de los nodos

			// Recorremos los nodos del elemento raiz
			for (int i = 0; i < nodeList.getLength(); i++) {
				attrs = nodeList.item(i).getAttributes(); // Guardamos los atributos del nodo
				if (nodeList.item(i) instanceof Text) { // Comprobamos si el nodo esta vacio

				} else {
					if (nodeList.item(i).getNodeName().equals("row")) { // Si el nodo es alumne mostramos nombre y su id
						String id = attrs.getNamedItem("id").getNodeValue();
						String position = attrs.getNamedItem("_position").getNodeValue();
						String address = attrs.getNamedItem("_address").getNodeValue();
						System.out.println(nodeList.item(i).getNodeName() + " id: " + id +
								" position: " + position + " address: " + address);
						
					} else { // Si el nodo es de otro tipo mostramos su nombre y su contenido
						System.out.println(espai + nodeList.item(i).getNodeName() + ": " + nodeList.item(i).getTextContent());
					}

					// Guardamos elemento del nodo e invocamos metodo de nuevo
					Element e =(Element) nodeList.item(i);
					mostrarNodes(e,espai+espai);	
				}
			}
		}
	// Metodo para aplicar la funcion setIdAttribute() a todos los elementos del fichero
	public static void idAttributeTrue(Document doc) {
		Element root = doc.getDocumentElement();
		NodeList nodeList = root.getChildNodes(); // Lista para guardar los nodos
		Element e = null;

		for (int i = 0; i < nodeList.getLength(); i++) {
			if("row".equals(nodeList.item(i).getNodeName())) {	
				e = (Element)nodeList.item(i);
				e.setIdAttribute("id", true);
			}
		}
	}
	// Metodo que devuelve la id del ultimo elemento del fichero
	public static int getLastId(Document doc) {
		Element root = doc.getDocumentElement();
		NodeList nodeList = root.getChildNodes(); // Lista para guardar los nodos
		String id = null;

		for (int i = 0; i < nodeList.getLength(); i++) {
			NamedNodeMap attrs = nodeList.item(i).getAttributes();
			if("row".equals(nodeList.item(i).getNodeName())) {	
				id = attrs.getNamedItem("id").getNodeValue();
			}
		}

		return Integer.parseInt(id);
	}
	// Metodo para confirmar las modificaciones a realizar en el fichero
	public static void confirmarModificaciones(Document doc, NodesXML2 node, Scanner teclado) throws TransformerException {
		while (true) {
			System.out.println("�Quieres confirmar modificaciones? (SI/NO)");
			String respuesta = teclado.nextLine();
			if (respuesta.equalsIgnoreCase("SI")) {
				saveData(doc,node); 	
				return;
			} else if (respuesta.equalsIgnoreCase("NO")) {
				return;
			} else {
				System.out.println("Opci�n incorrecta");
			}
		}
	}
	// Metodo para guardar las modificaciones en el fichero
	public static void saveData(Document doc, NodesXML2 node) throws TransformerException {
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();
		DOMSource domSource = new DOMSource(doc);
		StreamResult streamResult = new StreamResult(new File("oficines2.xml"));
		transformer.transform(domSource, streamResult);
		//DOMSource source = new DOMSource(doc);

	}
}
