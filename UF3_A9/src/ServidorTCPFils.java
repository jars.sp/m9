import java.net.*;
import java.io.*;
// Implementamos interface Runnable
public class ServidorTCPFils implements Runnable{
	private Socket client;
	private static int numClient = 0;
	// En el constructor pasamos el socket del cliente
	public ServidorTCPFils(Socket client) {
		this.client = client;

	}
	// En el m�todo run() controlamos la entrada y la salida del cliente
	@Override
	public void run() {

		try {
			String cadena = "";

			//FLUX DE SORTIDA AL CLIENT
			PrintWriter fsortida = new PrintWriter(this.client.getOutputStream(), true);

			//FLUX D'ENTRADA DEL CLIENT
			BufferedReader fentrada = new BufferedReader(new InputStreamReader(this.client.getInputStream()));

			try { // Guardamos el mensaje del cliente  en la variable cadena
				while ((cadena = fentrada.readLine()) != null) {

					fsortida.println(cadena); // Enviamos la respuesta al cliente
					System.out.println("Rebent: "+cadena); // Imprimimos el mensaje recibido
					if (cadena.contains("bye")) { // Si el mensaje recibio contiene "bye" cerramos conexi�n cliente
						this.client.close();
						numClient--;
					}

				}	

				fentrada.close();
				fsortida.close();
			}
			catch (Exception SocketException) {
				System.out.println("Conexi� tancada");
			}

		} catch (IOException e) {

			e.printStackTrace();
		}

	}
	// M�todo para aceptar las conexiones de clientes
	// Lo sincronizamos para controlar conexiones al mismo tiempo
	public static synchronized void conectar(Socket clientConnectat, ServerSocket servidor) {
		// Creamos un while para aceptar peticiones de varios clientes
		while (true) {
			try {
				clientConnectat = servidor.accept();
				numClient++;
			} catch (IOException e) {
				System.out.println("I/O error: " + e);
			}
			// Creamos un hilo por cada cliente
			ServidorTCPFils client = new ServidorTCPFils(clientConnectat);
			Thread fil = new Thread(client);
			fil.setName("Client " + numClient);
			fil.start();
		}
	}

	public static void main (String[] args) throws Exception {

		int numPort = 60000; // Variable para el n�mero del puerto
		ServerSocket servidor = new ServerSocket(numPort); // Socket para el servidor
		System.out.println("Esperant connexi�... ");
		Socket clientConnectat = null; // Socket para cliente
		// Aceptamos conexi�n
		conectar(clientConnectat, servidor);


		//System.out.println("Tancant connexi�... ");
		//servidor.close();

	}


}




