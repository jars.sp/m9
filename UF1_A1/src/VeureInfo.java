import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.FileTime;
import java.util.Date;

public class VeureInfo {

	public static void main(String[] args) {
		// Introducimos la ruta del fichero o directorio por par�metro
		String path = args[0];
		// Declaramos variable de referencia tipo File con la ruta anterior
		File f = new File(path);
		// Comprobamos si el fichero o directorio exite
		if(f.exists()) { // Si existe

			// Comprobamos si est� oculto o no
			if(f.isHidden()) {
				System.err.println("El objeto est� oculto");
			} else {
				System.err.println("El objeto no est� oculto");
			}

			if(f.isDirectory()) { // Si es directorio
				System.out.println("Fitxers al directori actual: ");
				String[] arxius = f.list(); // Listamos el contenido y lo guardamos en un array
				// Mostramos el contenido del array
				for (int i = 0; i<arxius.length; i++){ 
					System.out.println(arxius[i]);
				}
			}

			if(f.isFile()) { // Si es un fichero
				System.out.println("INFORMACI� SOBRE EL FITXER");
				System.out.println("Nom del fitxer : "+f.getName()); // Obtenemos el nombre
				System.out.println("Ruta           : "+f.getPath()); // Obtenemos ruta
				System.out.println("Ruta absoluta  : "+f.getAbsolutePath()); // Obtenemos ruta absoluta
				System.out.println("Es pot escriure: "+f.canRead()); // Comprobamos si se puede escribir
				System.out.println("Es pot llegir  : "+f.canWrite()); // Comprobamos si se puede leer
				System.out.println("Grandaria      : "+f.length()); // Obtenemos tama�o
				System.out.println("Es un directori: "+f.isDirectory()); // Comprobamos si es directorio
				System.out.println("Es un fitxer   : "+f.isFile()); // Comprobamos si es fichero
			}
			try {
				Date fecha = new Date(); // Obtenemos fecha actual
				FileTime modifiedTime = Files.getLastModifiedTime(f.toPath()); // Obtenemo fecha de ultima modificacion del objeto
				double diferencia = (fecha.getTime() - modifiedTime.toMillis())/1000; // Calculamos diferencia y la pasamos a segundos
				System.out.println("Ultima modificacion: " + modifiedTime);
				System.out.println("Fecha actual: " + fecha);
				if(diferencia < 259200) { // Si la diferencia es menor a tres dias
					System.out.println("El objeto se ha modificado dentro de los tres ultimos dias");
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
		} else { // Si no existe
			System.out.println("El fitxer o directori no existeix");
		}

	}

}
