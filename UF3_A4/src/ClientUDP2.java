import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class ClientUDP2 {

	public static void main (String[] args) throws Exception {

		//FLUX PER A ENTRADA EST�NDARD
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		// Se abre un Socket client
		DatagramSocket clientSocket = new DatagramSocket();
		byte[] enviats = new byte[1024]; // Array de bytes para datos enviados
		byte[] rebuts = new byte[1024]; // Array de bytes para datos recibidos

		//DADES DEL SERVIDOR al qual s'envia el missatge
		InetAddress IPServidor = InetAddress.getLocalHost(); // Se obtiene la IP del servidor
		int port = 9800; // Variable para el puerto
		
		try { // Se abre un try catch para atrapar el error del Timeout del servidor
			while (true) {
				//INTRODUIR DADES PEL TECLAT
				// Guardamos los datos enviados en bytes
				System.out.print("Introdueix missatge: ");
				String cadena = in.readLine();
				enviats = cadena.getBytes();

				//ENVIANT DATAGRAMA AL SERVIDOR
				System.out.println("Enviant "+enviats.length+"bytes al servidor.");
				// Se crea un datagrama del paquete a enviar
				// con los datos, la longitud de los datos, la IP del server y el puerto.
				DatagramPacket enviament = new DatagramPacket(enviats, enviats.length, IPServidor, port);
				// Se env�a el paquete con el m�todo send()
				clientSocket.send(enviament);

				//REBENT DATAGRAMA DEL SERVIDOR
				// Se crea un datagrama del paquete recibido desde el servidor
				// con los datos recibidos y su longitud
				DatagramPacket rebut = new DatagramPacket(rebuts, rebuts.length);
				// Con el m�todo setSoTimeot() indicamos el tiempo l�mite de espera
				// para que el servidor responda
				clientSocket.setSoTimeout(5000);
				System.out.println("Esperant datagrama...");
				clientSocket.receive(rebut);
				// Guardamos la info del datagrama recibido en un String
				String majuscula = new String(rebut.getData());

				//ACONSEGUINT INFORMACI� DEL DATAGRAMA
				InetAddress IPOrigen = rebut.getAddress(); // Se guarda IP del servidor
				int portOrigen = rebut.getPort(); // Se guarda puerto del servidor
				System.out.println("\tProcedent de: "+IPOrigen+":"+portOrigen); // Se muestra IP y puerto del servidor
				System.out.println("\tDades: "+majuscula.trim()); // Se muestra mensaje recibido

			}
		} catch (Exception SocketTimeoutException) { // Excepci�n si se acaba el tiempo de respuesta
			System.err.println("Receive timed out");
		}
		//Tanca el socket
		clientSocket.close();

	}

}


