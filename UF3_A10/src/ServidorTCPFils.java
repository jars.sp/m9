import java.net.*;
import java.util.Arrays;
import java.util.Scanner;
import java.io.*;
// Implementamos interface Runnable
public class ServidorTCPFils implements Runnable{

	private Socket client; // Variable para guardar el socket del cliente
	private String name;
	private int idSocket; // Variable para asignar in Id al socket
	private static int numClient = 0; // Variable para indicar el n�mero del cliente
	private static Thread fil; // Variable de hilo
	private static Socket[] sockets = new Socket[numClient+1]; // Array para almacenar todos los sockets
	private static String[] names = new String[numClient+1]; // Arrays para almacenar los nombres de clientes
	
	// En el constructor pasamos el socket del cliente 
	public ServidorTCPFils(Socket client) {
		this.client = client;

	}
	// M�todo para asignar id de Socket
	public void setIdSocket(int id) {
		idSocket = id;
	}
	// M�todo para obtener id de Socket
	public int getIdSocket() {
		return this.idSocket;
	}
	// M�todo para eliminar un cliente cuando se deconecta
	public void eliminarClient() {
		numClient--; // Decrementamos variable de clientes

		if (numClient > 0) {
			int counter = 0;
			// Creamos array auxiliares de nombres y sockets
			Socket[] auxSocket = new Socket[numClient];
			String[] auxName = new String[numClient];
			// Guardamos los valores nombre /socket menos el del cliente desconectado
			for (int i = 0; i < sockets.length; i++){
				if (getIdSocket() != i) {
					auxSocket[counter] = sockets[i];
					auxName[counter] = names[i];
					counter++;
				}

			}
			// Reiniciamos arrays sockets y names
			sockets = auxSocket;
			names = auxName;
			idSocket--;
		}
	}

	// En el m�todo run() controlamos la entrada y la salida del cliente
	@Override
	public void run() {

		try {

			String cadena = "";
			String mensaje;
			String cliente;

			//FLUX D'ENTRADA DEL CLIENT
			BufferedReader fentrada = new BufferedReader(new InputStreamReader(this.client.getInputStream()));
			//FLUX DE SORTIDA AL CLIENT
			PrintWriter fsortida = null;
			try { // Guardamos el mensaje del cliente  en la variable cadena
				while ((cadena = fentrada.readLine()) != null) {
					// Si cadena comienza por '^' guardamos el nombre en el array
					if (cadena.charAt(0)  == '^') {
						name = cadena.substring(1);
						names[numClient-1] = name;
						fil.setName(name); // Asignamos nombre al hilo
						System.out.println("Rebent: "+ name +  " " + cadena);
						//FLUX DE SORTIDA AL CLIENT
						fsortida = new PrintWriter(this.client.getOutputStream(), true);
						fsortida.println("Nombre asignado " + name); // Enviamos la respuesta al cliente
						
						// Env�o a un cliente en concreto si cadena comienza por '*'
					} else if (cadena.charAt(0)  == '*') {
						
						mensaje = cadena.substring(cadena.indexOf('_')+1); // Substraemos el mensaje
						cliente = cadena.substring(1,cadena.indexOf('_')); // Substremos nombre destinatario
						int checker = 0;
						System.out.println("Rebent: "+ name +  " " + cadena);
						// Buscamos destinatario en array de nombres
						for (int i = 0; i < names.length; i++) {
							// Si coincide le enviamos el mensaje
							if (cliente.equalsIgnoreCase(names[i])) {
								checker = -1;

								fsortida = new PrintWriter(this.sockets[i].getOutputStream(), true);
								fsortida.println(name + " to " + cliente + " " + mensaje); // Enviamos la respuesta al cliente
							} 

						}
						if (checker == 0) {
							fsortida = new PrintWriter(this.client.getOutputStream(), true);
							fsortida.println("Nombre de cliente incorrecto"); // Enviamos la respuesta al cliente
							
						}
						// Mostramos todos los clientes
					} else if (cadena.charAt(0)  == '+') {

						System.out.println("Rebent: "+ name +  " " + cadena);

						fsortida = new PrintWriter(this.client.getOutputStream(), true);
						fsortida.println("Clientes conectados " + Arrays.toString(names)); // Enviamos mensaje al cliente


					} else { // Env�o a todos los clientes
						System.out.println("Rebent: "+ name +  " " + cadena);
						for (int i = 0; i < sockets.length; i++) {

							//FLUX DE SORTIDA AL CLIENT
							fsortida = new PrintWriter(this.sockets[i].getOutputStream(), true);
							fsortida.println(name + " " + cadena); // Enviamos mensaje al cliente

						}

					}

					if (cadena.contains("bye")) { // Si el mensaje recibio contiene "bye" cerramos conexi�n cliente
						System.out.println("Rebent: "+ name +  " " + cadena); // Imprimimos el mensaje recibido
						this.client.close();
						
					}

				}	

				fentrada.close();
				fsortida.close();

			}
			catch (Exception SocketException) {
				System.out.println("Conexi� tancada");
				eliminarClient();

				if (numClient < 1) {
					System.out.println("Servidor desconectat");
					System.exit(0);
				}
			}


		} catch (IOException e) {

			e.printStackTrace();
		}

	}

	// M�todo para aceptar las conexiones de clientes
	// Lo sincronizamos para controlar conexiones al mismo tiempo
	public synchronized static void conectar(Socket clientConnectat, ServerSocket servidor) throws IOException {

		try {
			// Aceptamos la conexi�n
			clientConnectat = servidor.accept();
			// Creamos arrays auxiliares Socket / Name
			Socket[] auxSocket = new Socket[numClient+1];
			String[] auxName = new String[numClient+1];

			// Creamos un hilo por cada cliente y le asignamos idSocket
			ServidorTCPFils client = new ServidorTCPFils(clientConnectat);
			client.setIdSocket(numClient);
			fil = new Thread(client);
			fil.start();
			// Cada vez que se conecta un cliente incrementamos arrays sockets / names 
			for (int i = 0; i < sockets.length; i++){
				auxSocket[i] = sockets[i];
				auxName[i] = names[i];
			}
			sockets = auxSocket;
			names = auxName;
			sockets[numClient] = clientConnectat;
			numClient++;




		} catch (IOException e) {
			System.out.println("I/O error: " + e);
		}


	}

	public static void main (String[] args) throws Exception {
		// Pedimos el total de clientes permitidos
		Scanner teclado = new Scanner(System.in);
		System.out.println("Introduce total de clientes permitidos ");
		int clients = teclado.nextInt();
		teclado.nextLine();

		int numPort = 60000; // Variable para el n�mero del puerto
		ServerSocket servidor = new ServerSocket(numPort); // Socket para el servidor
		System.out.println("Esperant connexi�... ");
		// Socket para cliente
		Socket clientConnectat = null;
		// Creamos un while para aceptar peticiones de varios clientes
		while (numClient < clients) {
			conectar(clientConnectat, servidor);
		}

	}


}




