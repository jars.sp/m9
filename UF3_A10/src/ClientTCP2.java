import java.net.*;
import java.io.*;

public class ClientTCP2 {

	public static void main (String[] args) throws Exception {

		String host = "localhost";
		int port = 60000;//Port remot

		try {
			Socket client = new Socket(host, port);

			//FLUX DE SORTIDA AL SERVIDOR
			PrintWriter fsortida = new PrintWriter(client.getOutputStream(), true);

			//FLUX D'ENTRADA AL SERVIDOR
			BufferedReader fentrada = new BufferedReader(new InputStreamReader(client.getInputStream()));

			//FLUX PER A ENTRADA EST�NDARD
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

			String cadena = "^Client 2";
			String eco = "";
			
			while (cadena != null) {

				//Enviament cadena al servidor
				fsortida.println("Client 2 "+ cadena);
				//Rebuda cadena del servidor
				eco = fentrada.readLine();
				System.out.println("  =>ECO: "+eco);
				System.out.println("Introdueix la cadena: ");
				//Lectura del teclat
				cadena = in.readLine();


			}
			fsortida.close();
			fentrada.close();
			System.out.println("Finalitzaci� de l'enviament...");
			in.close();

			client.close();
		} catch(Exception SocketException) {
			System.out.println("Sessi� tancada");
		} 


	}

}

