import java.net.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;
// Implementamos Runnable para crear un hilo que lee los mensajes
public class ClientTCP3 implements Runnable {
	private Socket client;
	private String eco;
	private static Thread fil;
	// En el constructor pedimos el socket del cliente
	public ClientTCP3(Socket client) throws IOException {
		eco = "";
		this.client = client;
	}
	// En el m�todo run() abrimos un BufferedReader para obtener mensaje del servidor
	// guardamos el mensaje en la variable eco
	// mostramos el mensaje
	@Override
	public void run() {
		while(true) {
			try {
				BufferedReader fentrada = new BufferedReader(new InputStreamReader(client.getInputStream()));
				eco = fentrada.readLine();
				
			} catch (SocketException e) {
				
				System.err.println("Conexi� tancada");
				System.exit(0);
			} catch (IOException e) {
				
				e.printStackTrace();
			} 
			
			
			System.out.println("  =>Mensaje recibido: "+eco);
		}
	}

	public static void main (String[] args) throws Exception {

		String host = "localhost";
		int port = 60000;//Port remot

		try {
			Socket client = new Socket(host, port);

			//FLUX DE SORTIDA AL SERVIDOR
			PrintWriter fsortida = new PrintWriter(client.getOutputStream(), true);

			//FLUX PER A ENTRADA EST�NDARD
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			
			// Creamos una variable de referencia Cliente
			ClientTCP3 cliente = new ClientTCP3(client);
			// Creamos hilo de cliente y lo iniciamos
			fil = new Thread(cliente);
			fil.start();
			// Creamos fecha de env�o de mensaje
			DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
			Date date = new Date();
			
			// Pedimos el nombre de cliente
			System.out.println("Nombre del cliente: ");
			String cadena = "^"+in.readLine(); 
		
			// Se env�a el nombre
			fsortida.println(cadena);
			
			// while para env�o de mensajes
			while (cadena != null) {

				System.out.println("Introdueix la cadena: ");
				System.out.println();
				//Lectura del teclat
				cadena = in.readLine();
				fsortida.println(cadena + " " + dateFormat.format(date));
				// Si el cliente se despide "bye", la conexi�n se finaliza
				if (cadena.equalsIgnoreCase("bye")) {
					fsortida.close();
					System.out.println("Finalitzaci� de l'enviament...");
					in.close();
					client.close();
				}
			}

			
		} catch(Exception SocketException) {
			System.out.println("Sessi� tancada");
			
			
		} 


	}


}

