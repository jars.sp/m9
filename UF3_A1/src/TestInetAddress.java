import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;

public class TestInetAddress {

	public static void main (String[] args) {
		
		// Se crean 4 argumentos
		String localHost = args[0]; // args para localhost
		String cisco = args[1]; // args para cisco
		String google = args[2]; // args para google
		String baixCamp = args[3]; // args para la web del inti
		
		InetAddress dir = null; // Se declara variable para Inet
		System.out.println("SORTIDA PER A LOCALHOST");

		try {
			//LOCALHOST args[0]
			dir = InetAddress.getByName(localHost); // Se hace un get por nombre para obtener Inet, 
													// en este caso del localhost
			provaTots(dir);
			System.out.println("=====================================================");
			
			//URL www.cisco.com args[1]
			//System.out.println("SORTIDA PER A URL");
			//			dir = InetAddress.getByName(cisco); // Se obtiene la IP del nombre "www.google.com"
			//			provaTots(dir);

			// Creamos ArrayList para guardar todos los arrays InetAddress[]
			ArrayList<InetAddress[]> adrecesList = new ArrayList<InetAddress[]>();
			// Con getAllByName obtenemos todas las Inets que responden a un nombre
			adrecesList.add(InetAddress.getAllByName(InetAddress.getByName(cisco).getHostName()));
			adrecesList.add(InetAddress.getAllByName(InetAddress.getByName(google).getHostName()));
			adrecesList.add(InetAddress.getAllByName(InetAddress.getByName(baixCamp).getHostName()));
			
			// Mostramos la informaci�n sobre las Inets y sus m�todos
			for (int i = 0; i < adrecesList.size(); i++) {
				System.out.println("SORTIDA PER A URL");
				for (int j = 0; j < adrecesList.get(i).length; j++) {
					System.out.println("\tAdreces IP per a: "+ adrecesList.get(i)[j].getHostName()); // Se imprime el nombre de la IP guardada
					System.out.println("\t"+adrecesList.get(i)[j].toString());
					provaTots(adrecesList.get(i)[j]);
					System.out.println("=====================================================");
				}

			}
			//Array tipus InetAddress amb totes les direcciones por nombre
			//			InetAddress[] adreces = InetAddress.getAllByName(dir.getHostName());
			//			for (int i=0; i<adreces.length; i++) {
			//				System.out.println("\t\t"+adreces[i].toString());
			//			}


		} catch (UnknownHostException e1) {e1.printStackTrace();}

	}

	private static void provaTots(InetAddress dir) {

		InetAddress dir2;

		System.out.println("\tM�tode getByName(): "+dir + "   // Hace una petici�n para obtener IP del nombre introducido");

		try {
			dir2 = InetAddress.getLocalHost();
			System.out.println("\tM�tode getLocalHost(): "+dir2 + "  // Obtiene la Inet del localhost");
		} catch (UnknownHostException e) {e.printStackTrace();}

		//FEM SERVIR M�TODES DE LA CLASSE
		System.out.println("\tM�tode getHostName(): "+dir.getHostName() + "  // Muestra el nombre de la Inet almacenada en la variable"); 
		System.out.println("\tM�tode getHostAddress(): "+dir.getHostAddress() + "   // Muestra la direcci�n IP de la Inet almacenada"); 
		System.out.println("\tM�tode toString(): "+dir.toString() + "   // Muestra la Inet completa (Name/Address)"); 
		System.out.println("\tM�tode getCanonicalHostName(): "+dir.getCanonicalHostName() + "  // Muestra el registro CNAME"); 
		System.out.println("\tM�tode hashCode(): "+dir.hashCode() + "  // Genera un Hash de la IP"); 
		System.out.println("\tM�tode isMulticastAddress(): "+dir.isMulticastAddress() + "  // Comprueba si la IP es multicast"); 
		System.out.println("\tM�tode isSiteLocalAddress(): "+dir.isSiteLocalAddress() + "  // Comprueba si la IP es local"); 
		System.out.println("\tM�tode getLoopbackAddress(): "+dir.getLoopbackAddress() + "  // Retorna la IP inversa"); 
	}

}



