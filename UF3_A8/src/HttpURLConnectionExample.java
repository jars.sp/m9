import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

public class HttpURLConnectionExample {

	private final String USER_AGENT = "Mozilla/5.0";

	public static void main(String[] args) throws Exception {
		String url = null;
		boolean salir = false;
		// Creamos objeto HttpURLConnectionExample para hacer peticiones al servidor
		HttpURLConnectionExample http = new HttpURLConnectionExample();
		Scanner teclado = new Scanner(System.in);
		// while para mostrar men� y seleccionar opciones
		while (!salir) {
			System.out.println("MEN�");
			System.out.println("1. GET");
			System.out.println("2. POST");
			System.out.println("3. EXIT");
			char opcion = teclado.next().charAt(0);
			teclado.nextLine();

			switch(opcion) {
			// Caso uno pide URL e invoca al m�todo Get
			case '1':
				System.out.println("Introduce la URL");
				url = teclado.nextLine();
				System.out.println("Testing 1 - Send Http GET request");
				http.sendGet(url);
				break;
			// Caso 2 pide URL e invoca al m�todo POST
			case '2':
				System.out.println("Introduce la URL");
				url = teclado.nextLine();
				System.out.println("\nTesting 2 - Send Http POST request");
				http.sendPost(url);
				break;
			// Caso 3 sale del programa
			case '3':
				salir = true;
				break;
			//  Por defecto muestra mensaje	
			default:
				System.out.println("Opci�n incorrecta");

			}

		}

		teclado.close();
	}

	// HTTP GET request
	private void sendGet(String url) throws Exception {
		
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		// optional default is GET
		con.setRequestMethod("GET");

		//add request header
		con.setRequestProperty("User-Agent", USER_AGENT);

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());

	}

	// HTTP POST request
	private void sendPost(String url) throws Exception {

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		//add reuqest header
		con.setRequestMethod("POST");
		con.setRequestProperty("User-Agent", USER_AGENT);
		con.setRequestProperty("Accept-Language", "ca-es");

		//Query string
		String urlParameters = "categoryid=7";

		// Send post request
		con.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = con.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		//print result
		System.out.println(response.toString());

	}

}
