import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
import javax.swing.*;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class NauEspaial extends javax.swing.JFrame {

	boolean left = false;
	boolean right = false;
	static boolean stop = false;

	public NauEspaial() {
		initComponents();
	}

	@SuppressWarnings("unchecked")
	private void initComponents() {
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		setBackground(new java.awt.Color(255, 255, 255));
		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 400, Short.MAX_VALUE));
		layout.setVerticalGroup(
				layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING).addGap(0, 300, Short.MAX_VALUE));
		pack();
	}

	public static void main(String args[]) {

		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception ex) {
			java.util.logging.Logger.getLogger(NauEspaial.class.getName()).log(java.util.logging.Level.SEVERE, null,
					ex);
		}

		NauEspaial f = new NauEspaial();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setTitle("Naus Espaials");
		f.setContentPane(new PanelNau());
		f.setSize(480, 560);
		f.setVisible(true);

	}

}

class PanelNau extends JPanel implements Runnable, KeyListener {

	// Nau[] nauEnemiga;
	Nau nauPropia;
	Vector<Nau> nauEnemiga = new Vector();
	Vector<Disparo> disparoPropio = new Vector();
	Vector<Disparo> disparoEnemigo = new Vector();
	private static boolean shot = false;

	public PanelNau() {
		// nauEnemiga = new Nau[Nau.numNaus];

		for (int i = 0; i < Nau.numNaus; i++) {
			Random rand = new Random();
			int velocitat = (rand.nextInt(3) + 4) * 10;
			int posX = rand.nextInt(100) + 30;
			int posY = rand.nextInt(100) + 30;
			int dX = rand.nextInt(3) + 1;
			int dY = rand.nextInt(3) + 1;
			String nomNau = Integer.toString(i);
			nauEnemiga.add(new Nau(nomNau, i, posX, posY, dX, dY, velocitat));
			
		}

		nauPropia = new Nau(200, 400, 0, 0, 5);

		Thread n = new Thread(this);
		n.start();

		// Creo listeners per a que el fil principal del programa gestioni
		// esdeveniments del teclat
		addKeyListener(this);
		setFocusable(true);

	}

	public void run() {
		System.out.println("Inici fil repintar");
		while (!NauEspaial.stop) {
			try {
				Thread.sleep(50);
			} catch (Exception e) {
			} // espero 0,1 segons
			System.out.println("Repintant");
			repaint();

		}
	}

	public void gameOver() {
		NauEspaial.stop = true;
		JOptionPane.showMessageDialog(this, "Game Over", "Game Over", JOptionPane.YES_NO_OPTION);
		System.exit(ABORT);
	}

	public void win() {
		NauEspaial.stop = true;
		JOptionPane.showMessageDialog(this, "You Win Congratulations!!", "WIN", JOptionPane.YES_NO_OPTION);
		System.exit(ABORT);
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		Set<Thread> threads = Thread.getAllStackTraces().keySet();

		// Imprimimos información de hilos
		for (Thread t : threads) {
			String name = t.getName();
			Thread.State state = t.getState();
			int priority = t.getPriority();
			String type = t.isDaemon() ? "Daemon" : "Normal";
			System.err.printf("%-20s \t %s \t %d \t %s\t %s\n", name, state, priority, type, t.getThreadGroup());
		}

		// Pintamos las naves enemigas
		for (int i = 0; i < nauEnemiga.size(); ++i) {

			// Si nave enemiga choca con nave Propia
			if (Math.sqrt((int) Math.pow(nauPropia.getX() - nauEnemiga.get(i).getX(), 2)
					+ (int) Math.pow(nauPropia.getY() - nauEnemiga.get(i).getY(), 2)) < 25) {

				gameOver();

			} else {
				nauEnemiga.get(i).pinta(g);

			}
		}

		// Pintamos nuestra nave
		nauPropia.pinta(g);

		// Si se ha disparado
		if (shot) {

			for (int i = 0; i < disparoPropio.size(); i++) {

				for (int j = 0; j < nauEnemiga.size(); j++) {
					// Si el disparo impacta con nave enemiga, removemos nave y disparo
					if (Math.sqrt((int) Math.pow(nauEnemiga.get(j).getX() - disparoPropio.get(i).getX(), 2)
							+ (int) Math.pow(nauEnemiga.get(j).getY() - disparoPropio.get(i).getY(), 2)) < 25) {

						Nau.numNaus--;
						nauEnemiga.remove(j);
						disparoPropio.remove(i);

					}

				}
				// Si el disparo llega al final lo removemos
				if (disparoPropio.get(i).getY() < 15) {
					disparoPropio.remove(i);

				} else {
					// Pintamos los disparos
					disparoPropio.get(i).pinta(g);
				}

			}
		}
		// Creamos un random para disparos enemigos
		int random = (int) (Math.random() * 15);

		// Si el random es igual a 0 disparamos
		if (random == 0) {
			disparar();
		}
		// for para pintar los disparos enemigos
		for (int i = 0; i < disparoEnemigo.size(); ++i) {
			// Comprobamos si disparo impacta con nauPropia
			if (Math.sqrt((int) Math.pow(nauPropia.getX() - disparoEnemigo.get(i).getX(), 2)
					+ (int) Math.pow(nauPropia.getY() - disparoEnemigo.get(i).getY(), 2)) < 20) {
				gameOver();

			}

			// Controlamos el recorrido de los disparos y removemos el disparo
			if (disparoEnemigo.get(i).getY() > 440) {
				disparoEnemigo.remove(i);
			} else {
				// Pintamos los disparos
				disparoEnemigo.get(i).pinta(g);
			}

		}
		// No quedan naves ganamos la partida
		if (Nau.numNaus == 0) {

			win();
		}

	}

	// Creamos un método para el disparo enemigo
	public synchronized void disparar() {

		// Hacemos un random con la longitud del array de las naves enemigas
		// Añadimos un disparo al Vector con X Y del random para que los disparos sean
		// aleatorios
		int random = (int) (Math.random() * nauEnemiga.size());
		Disparo disparEnemi = new Disparo(Integer.toString(nauEnemiga.size()), nauEnemiga.get(random).getX() + 15,
				nauEnemiga.get(random).getY() + 15, 0, +2, 10);
		disparoEnemigo.add(disparEnemi);

	}

	// Metodes necesaris per gestionar esdeveniments del teclat
	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {

		// Left
		if (e.getKeyCode() == 37) {
			nauPropia.esquerra();
		}

		// Right
		if (e.getKeyCode() == 39) {
			nauPropia.dreta();
		}

		// UP
		if (e.getKeyCode() == 38) {
			nauPropia.up();
		}
		// down
		if (e.getKeyCode() == 40) {
			nauPropia.down();
		}

		// Shot
		// Se comprueba si se pulsa espacio
		if (e.getKeyCode() == 32) {
			// Se crea un nuevo disparo con las coordenadas de nuestra nave propia
			// Se indica avance superior automático en -2 puntos
			Disparo dispar = new Disparo(nauPropia.getX() + 15, nauPropia.getY(), 0, -2, 10);
			disparoPropio.add(dispar);
			// Se cambia booleano shot a true
			shot = true;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
	}
}

class Nau extends Thread {
	private String nomNau;
	static int numNaus = 10;
	private int numero;
	private int x, y;
	private int dsx, dsy, v;
	private int tx = 10;
	private int ty = 10;

	// Se declaran dos variables tipo Image
	// Una para nave propia y otra para naves enemigas
	private String imgEnemiga = "nauEnemiga.png";
	private Image imageEnemiga;
	private String imgPropia = "nau.png";
	private Image imagePropia;

	// Creamos dos grupos para las naves
	ThreadGroup nausEnemigas = new ThreadGroup("nausEnemigas");
	ThreadGroup nauPropia = new ThreadGroup("nausPropia");
	// Se declara variable de contador
	static int countNau;

	// Constructor para naves enemigas
	public Nau(String nomNau, int numero, int x, int y, int dsx, int dsy, int v) {
		this.nomNau = nomNau;

		this.numero = numero;
		this.x = x;
		this.y = y;
		this.dsx = dsx;
		this.dsy = dsy;
		this.v = v;
		imageEnemiga = new ImageIcon(Nau.class.getResource("nauEnemiga.png")).getImage();
		// Se crea un hilo por nave y se añade al grupo
		//Thread nau = new Thread(this, "Nau Enemiga " + countNau);
		Thread nau = new Thread(nausEnemigas,this,"Nau Enemiga " + countNau);
		nausEnemigas = nau.getThreadGroup();
		nau.setPriority(2);
		nau.start();
		countNau++;

	}

	// Constructor para nave propia
	public Nau(int x, int y, int dsx, int dsy, int v) {

		this.numero = numero;
		this.x = x;
		this.y = y;
		this.dsx = dsx;
		this.dsy = dsy;
		this.v = v;
		imageEnemiga = new ImageIcon(Nau.class.getResource("nau.png")).getImage();
		Thread nau = new Thread(nauPropia, this,"Nau Propia");
		nau.setPriority(3);
		nau.start();
		
	}

	public int velocitat() {
		return v;
	}

	public synchronized void moure() {
		x = x + dsx;
		y = y + dsy;
		// si arriva als marges ...
		if (x >= 450 - tx || x <= tx)
			dsx = -dsx;
		if (y >= 500 - ty || y <= ty)
			dsy = -dsy;
	}

	public synchronized void pinta(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(this.imageEnemiga, x, y, null);
		g2d.drawImage(this.imagePropia, x, y, null);
	}

	public void run() {
		while (!NauEspaial.stop) {

			try {
				Thread.sleep(this.v);
			} catch (Exception e) {
			}
			moure();
		}
	}

	public void esquerra() {
		this.x -= 7;
		// this.dsx = -10;
	}

	public void dreta() {
		this.x += 7;
		// this.dsx = 10;
	}

	public void up() {
		this.y -= 7;
		// this.dsx = -10;
	}

	public void down() {
		this.y += 7;
		// this.dsx = 10;
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}
}

class Disparo extends Thread {
	static int numDisparos;
	private String nomDisparo;
	private int numero;
	private int x, y;
	private int dsx, dsy, v;
	private int tx = 10;
	private int ty = 10;

	// Declaramos dos variable tipo Image para disparo propio y enemigo
	private String imgDisparoPropio = "spriteNau.gif";
	private Image imageDisparoPropio;
	private String imgDisparoEnemigo = "spriteEnemigo.png";
	private Image imageDisparoEnemigo;

	// Creamos dos grupos para disparos
	ThreadGroup disparosPropios = new ThreadGroup("disparosPropios");
	ThreadGroup disparosEnemigos = new ThreadGroup("disparosEnemigos");
	
	// Contador de disparos
	static int countPropios;
	static int countEnemigos;

	// Constructor para disparos propios
	public Disparo(int x, int y, int dsx, int dsy, int v) {

		this.numero = numero;
		this.x = x;
		this.y = y;
		this.dsx = dsx;
		this.dsy = dsy;
		this.v = v;

		imageDisparoPropio = new ImageIcon(Nau.class.getResource("spriteNau.gif")).getImage();
		Thread disparo = new Thread(disparosPropios,this, "Disparo Propio " + countPropios);
		disparo.setPriority(1);
		disparo.start();
		countPropios++;
	}

	// Constructor para disparos enemigos
	public Disparo(String nomDisparo, int x, int y, int dsx, int dsy, int v) {
		this.nomDisparo = nomDisparo;
		this.numero = numero;
		this.x = x;
		this.y = y;
		this.dsx = dsx;
		this.dsy = dsy;
		this.v = v;

		imageDisparoEnemigo = new ImageIcon(Nau.class.getResource("spriteEnemigo.png")).getImage();
		Thread disparo = new Thread(disparosEnemigos,this, "Disparo Enemigo " + countEnemigos);
		disparo.setPriority(1);
		disparo.start();
		countEnemigos++;
	}

	public int velocitat() {
		return v;
	}

	public synchronized void moure() {
		x = x + dsx;
		y = y + dsy;
		// si arriva als marges ...
		if (x >= 450 - tx || x <= tx)
			dsx = -dsx;
		if (y >= 500 - ty || y <= ty)
			dsy = -dsy;
	}

	public synchronized void pinta(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.drawImage(this.imageDisparoPropio, x, y, null);
		g2d.drawImage(this.imageDisparoEnemigo, x, y, null);
	}

	public void run() {
		while (!NauEspaial.stop) {

			try {
				Thread.sleep(this.v);
			} catch (Exception e) {
			}
			moure();
		}
	}

	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getTy() {
		return ty;
	}
}
