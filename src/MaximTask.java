import java.util.concurrent.RecursiveTask;
import java.util.concurrent.ForkJoinPool;

// Clase de tipo RecursiveTask
public class MaximTask extends RecursiveTask<Short> {
		
	// Variables de clase
	private static final int LLINDAR=10000000;
	private short[] arr ;
	private int inici, fi;
	static int comptador = 0;

	// Constructor de la clase
	public MaximTask(short[] arr, int inici, int fi) {
		this.arr = arr;
		this.inici = inici;
		this.fi = fi;
	}
	
	/* Calcula el m�ximo secuencialmente
	 * mientras se cumpla la condici�n del compute()
	 * En otro caso se ejecutar� recursivamente
	 */
	private short getMaxSeq(){
		short max = arr[inici];
		
		for (int i = inici+1; i < fi; i++) {
			
			if (arr[i] > max) {
				max = arr[i];
				
			}
		}
		return max;
	}
	
	// C�lculo del m�ximo recursivamente
	/* Divide el array short en dos bloques
	 * mientra se cumpla la condici�n  del compute()
	 * en otro caso se ejecutar� secuencialmente
	 */
	private short getMaxReq(){
		MaximTask task1;
		MaximTask task2;
		int mig = (inici+fi)/2+1;
		task1 = new MaximTask(arr, inici, mig);
		task1.fork();
		task2 = new MaximTask(arr, mig, fi);
		task2.fork();
		
		// Con el join no continuar� una tarea hasta que no termine la otra
		return (short) Math.max(task1.join(), task2.join());
	}

	/* compute() para ejecuci�n de tareas
	 * Contiene condici�n para ejecuci�n
	 * secuencial o recursiva
	 */
	@Override
	protected Short compute() {
		
		if(fi - inici <= LLINDAR){ // Si (fi-inici) es menor o igual al Llindar se ejecutar� secuencialmente
			comptador++;
			System.out.println("Comptador secuencial " + comptador + "  " + "Inici " + inici + "       Fi " + fi);
			return getMaxSeq();
		}else{	// Si es mayor se ejecutar� recursivamente
			comptador++;
			System.out.println("Comptador recursivo " + comptador + "  " + "Inici " + inici + "         Fi " + fi);
			return getMaxReq();
		}
	}

	public static void main(String[] args) {

		short[] data = createArray(50000000);
		
		// Mira el n�mero de processadors
		System.out.println("Inici c�lcul");
		ForkJoinPool pool = new ForkJoinPool();

		// Se crea MaximTask y se le indica inicio y fin
		int inici=0;
		int fi= data.length;
		MaximTask tasca = new MaximTask(data, inici, fi);

		long time = System.currentTimeMillis();
		// crida la tasca i espera que es completin
		int result1 = pool.invoke(tasca);
		
		// m�xim
		int result= tasca.join();
		
		System.out.println("Temps utilitzat:" +(System.currentTimeMillis()-time));

		System.out.println ("M�xim es " + result);
	}

	// Creaci�n de array con longitud aleatoria
	private static short [] createArray(int size){
		short[] ret = new short[size];
		for(int i=0; i<size; i++){
			ret[i] = (short) (1000 * Math.random());
			if(i==((short)(size*0.9))){
	
				ret[i]=1005;
			}
		}
		return ret;
	}
  }