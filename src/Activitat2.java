
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Activitat2 {
	

	public static void main(final String... args) throws InterruptedException, ExecutionException {
	
		//mostrem hora actual abans d�execuci�
		Calendar calendario = new GregorianCalendar();
		System.out.println("Inici: "+ calendario.get(Calendar.HOUR_OF_DAY) + ":" + calendario.get(Calendar.MINUTE) +
				":" + calendario.get(Calendar.SECOND));
		
		// Crea un pool de 4 hilos para distribuir las tareas a ejecutar
		final ScheduledExecutorService schExService = Executors.newScheduledThreadPool(4);
		
		// Crea objeto Runnable para ejecutar la tarea.
		final Runnable ob = new Activitat2().new ExecutaFil();
		
		// Con schExService.scheduleWithFixedDelay indicaremos cuando inicia la ejecuci�n
		// del primer hilo y el intervalo entre el siguiente
		schExService.scheduleWithFixedDelay(ob, 5, 6, TimeUnit.SECONDS);
		
		// Con schExService.awaitTermination() indicamos el tiempo que
		// queremos que transcurra hasta la finalizaci�n de la ejecuci�n 
		schExService.awaitTermination(30, TimeUnit.SECONDS);
		
		// Con shutdown cerramos la ejecuci�n de hilos .
		schExService.shutdownNow();
		System.out.println("Completat");
	}

	// Se crea clase que implementa la interfaz Runnable para ejecuci�n con hilos
	// Fil Runnable
	class ExecutaFil implements Runnable {
		@Override
		public void run() { // Se indica la tarea a realizar dentro del m�todo run()
			Calendar calendario = new GregorianCalendar();
			System.out.println("Hora execuci� tasca: "+
					calendario.get(Calendar.HOUR_OF_DAY) + ":" +
					calendario.get(Calendar.MINUTE) + ":" +
					calendario.get(Calendar.SECOND));
			System.out.println("Tasca en execuci�");
			System.out.println("Execuci� acabada");
			System.out.println(Thread.currentThread().getName());
		}
	}
}



