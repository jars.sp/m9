import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

// Se crea una clase para el c�lculo de Fibonacci
// mediante el uso de la interfaz Recursive Task con hilos
public class A_fibonacci_forkJoin extends RecursiveTask<Long> {
   
	long numero;
	
	// Se crea constructor con p�rametro long
	// para indicar la posici�n del n�mero Fibonacci
	public A_fibonacci_forkJoin(long numero){
		this.numero=numero;
	}    

	// Se crea m�todo compute para ejecutar la tarea
	@Override
	protected Long compute() {
		
		double calcul = java.lang.Math.cos(54879854);
		if(numero <= 1) return numero;

		// Obtenemos posici�n - 1
		A_fibonacci_forkJoin fib1 = new A_fibonacci_forkJoin(numero-1);
		//fib1.fork();
		
		// Obtenemos posici�n - 2 y creamos un fork
		A_fibonacci_forkJoin fib2 = new A_fibonacci_forkJoin(numero-2);
		fib2.fork();
		
		// Retornamos el valor de la posici�n Fibonacci indicada
		// fib1.compute hilo inicial
		// fib.join hilo del fork
		return fib1.compute()+ fib2.join();
	}

	// M�todo Fibonacci recursivo sin hilos
	public static long calculaFibonacci(long numero) {
        double calcul = java.lang.Math.cos(54879854);
        if (numero==0) { return 0; }
        else if (numero==1) { return 1; }
        else {
            return (calculaFibonacci(numero-2) + calculaFibonacci(numero-1));
        }
    }

	public static void main(String[] args){
		
		ForkJoinPool pool = new ForkJoinPool();
		
		// Se invoca al m�todo con hilos
		double startFils = System.currentTimeMillis();
		System.out.println("Calculat amb fils:  " + pool.invoke(new A_fibonacci_forkJoin(36))); 
		double endFils = System.currentTimeMillis();
		System.out.println((endFils - startFils) / 1000);
		
		// Se invoca al m�todo sin hilos
		double start = System.currentTimeMillis();
		System.out.println("Calculat sense fils:  " + calculaFibonacci(36));
		double end = System.currentTimeMillis();
		System.out.println((end - start) / 1000);
		
	}
}



