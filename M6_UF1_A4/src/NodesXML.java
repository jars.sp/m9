import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;

public class NodesXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		// per a carregar en mem�ria un arxiu xml
		File file = new File("alumnes.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);

		//per obtenir el node arrel
		Element nodeArrel = doc.getDocumentElement();
		String espai = "  ";
		mostrarNodes(nodeArrel, espai);

	}
	public static void mostrarNodes(Element nodeArrel, String espai) {
		NodeList nodeList = nodeArrel.getChildNodes(); // Lista para guardar los nodos
		Element e = null; // Elemento para guardar los elementos hijos
		String nodeContent = null; // Variable para guardar el contenido de los nodos
		String id = null; // Variable para guardar el valor del atributo id
		// Recorremos los nodos del elemento raiz
		for (int i = 0; i < nodeList.getLength(); i++) {
			// Si recibimos una instacia #text saltamos ese nodo
			if (nodeList.item(i) instanceof Text) {

			} else { // Si el nodo tiene contenido
				// Comprobamos que el nodo sea de tipo ELEMENT_NODE
				if(nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
					nodeContent = nodeList.item(i).getTextContent(); // Guardamos el contenido del nodo
					if("alumne".equals(nodeList.item(i).getNodeName())) { // Si el nodo es alumne
						id = nodeList.item(i).getAttributes().getNamedItem("id").getNodeValue(); // Guardamos valor de id
						System.out.println(nodeList.item(i).getNodeName() + " " + id); // Se imprime nombre del nodo y su id
					} else { // Si el nodo no es alumne se imprime el nombre y su contenido
						System.out.println(espai + nodeList.item(i).getNodeName() + " "+ nodeContent);

					}
					// Guardamos el elemento de cada nodo e invocamos al m�todo
					e =(Element) nodeList.item(i);
					mostrarNodes(e,espai+espai);
				} 


			}

		}
	}

}
