import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;

public class NodesXML2 {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		// per a carregar en mem�ria un arxiu xml
		File file = new File("oficines.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file);

		//per obtenir el node arrel
		Element nodeArrel = doc.getDocumentElement();
		String espai = "  ";
		mostrarNodes(nodeArrel, espai);

	}
	public static void mostrarNodes(Element nodeArrel, String espai) {
		NodeList nodeList = nodeArrel.getChildNodes(); // Lista para guardar los nodos
		Element e = null; // Elemento para guardar los elementos hijos
		String nodeContent = null; // Variable para guardar el contenido de los nodos
		NamedNodeMap attrs; // NamedNodeMap para guardar atributos de los nodos
		String id; // Variable para atributo id
		String uuid; // Variable para atributo uuid
		String position; // Variable para atributo position
		String address; // Variable para atributo address
		// Recorremos los nodos del elemento raiz
		for (int i = 0; i < nodeList.getLength(); i++) {
			attrs = nodeList.item(i).getAttributes(); // Guardamos atributos de nodo
			// Comprobamos que el nodo sea de tipo ELEMENT_NODE
			if(nodeList.item(i).getNodeType() == Node.ELEMENT_NODE) {
				nodeContent = nodeList.item(i).getTextContent(); // Guardamos el contenido del nodo

				if("row".equals(nodeList.item(i).getNodeName())) { // Si el nodo es row imprimimos nombre y sus atributos
					// Guardamos los atributos en sus correspondientes variables
					id = attrs.getNamedItem("_id").getNodeValue();
					uuid = attrs.getNamedItem("_uuid").getNodeValue();
					position = attrs.getNamedItem("_position").getNodeValue();
					address = attrs.getNamedItem("_address").getNodeValue();
					System.out.println(nodeList.item(i).getNodeName() + " id: " + id + " uuid: " + uuid + " podition: " + position + " address: " + address);
				} else { // Si no es row imprimos nombre y su contenido
					System.out.println(espai + nodeList.item(i).getNodeName() + " "+ nodeContent);
				}
				// Guardamos elemento del nodo e invocamos metodo
				e =(Element) nodeList.item(i);
				mostrarNodes(e,espai+espai);	

			}

		}

	}

}
