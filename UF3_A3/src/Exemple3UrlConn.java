import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;



public class Exemple3UrlConn {

	@SuppressWarnings("rawtypes")
	public static void main (String[] args) {

		try {
			String cadena;
			String address = args[0];
			String num = args[1];
			String script = args[2];
			// Constru�m la URL amb els arguments
			URL url = new URL(address);
			// Establim connexi� amb la URL
			URLConnection connexio = url.openConnection();

			System.out.println("===============================================================");
			System.out.println("ADRE�A, DATA I CONTINGUT");
			System.out.println("Adre�a [getURL]: " + connexio.getURL());
			// Obtenim la data de l'�ltima connexi�
			Date data = new Date(connexio.getLastModified());
			System.out.println("Data �ltima modificaci� [getLastModified()]: " + data);
			System.out.println("Tipus de Contingut [getContentType()]" + connexio.getContentType());

			System.out.println("===============================================================");
			System.out.println("TOTS ELS CAMPS DE CAP�ALERA AMB getHeaderFields(): ");

			//Fem servir una estructura Map per a recuperar cap�aleres
			Map campsCap�alera = connexio.getHeaderFields();
			// Fem servir una Iterator on desarem totes les entrades
			Iterator it = campsCap�alera.entrySet().iterator();
			// Mostrem les entrades
			while (it.hasNext()) {
				Map.Entry map = (Map.Entry) it.next();
				System.out.println(map.getKey() + ":" + map.getValue());
			}

			// Mostrem els 5 primers camps de la Cal�alera mediant l'argument num
			System.out.println("===============================================================");
			System.out.println("Camps 1 al 5 de Cap�alera");
			for (int i = 0; i < Integer.parseInt(num); i++) {
				System.out.println("getHeaderField("+(i+1)+")=> " + connexio.getHeaderField(i));

			}
			System.out.println("===============================================================");

			// Mostrem el contigut de la part de la web que conte l'argument script
			System.out.println("Contingut dels scripts de [url.getFile()]: " + url.getFile());
			// Abrim un BufferedReader per deisar l'input obtingut de la URL
			BufferedReader pagina = new BufferedReader(new InputStreamReader(url.openStream()));
			// Mostrem el inputStream desat

			while ((cadena = pagina.readLine()) != null) {
				if (cadena.contains(script)) { // Si el contingut conte la paraula script
					System.out.println(cadena);
				}
			}
			// Provem el metode isGifFormat() amb dues URL amb gif i sense gif
			System.out.println("El contingut de la url no �s un gif " + isGifFormat(url));
			URL url2 = new URL("https://en.wikipedia.org/wiki/GIF#/media/File:Rotating_earth_(large).gif");
			System.out.println("El contingut de la url �s un gif " + isGifFormat(url2));

		}
		catch (MalformedURLException e) { e.printStackTrace();}
		catch (IOException e) {e.printStackTrace();}
	}
	// Metode que comprova si el contingut de la URL es un gif
	static boolean isGifFormat(URL url){
		boolean ret = false;
		try {
			URLConnection con = url.openConnection();
			String headerType = con.getContentType(); // Retorna el tipus de contingut
			String guessType = url.getFile(); // Elimino guessContentTypeFromName perque retorna null i dona error
			System.out.println(headerType);
			System.out.println(guessType);
			ret = headerType.endsWith("gif") || headerType.endsWith("GIF") ||
					guessType.endsWith("GIF") || guessType.endsWith("gif");
		} catch (IOException ex) {
			Logger.getLogger(Exemple3UrlConn.class.getName()).log(Level.
					SEVERE, null, ex);
		}
		return ret;
	}

}

